/* eslint-disable functional/no-let */
/* eslint-disable sort-keys */
import moment from 'moment';

export function convertETHToUSD(eth, converter) {
  const { USD = '' } = converter;
  const val = (eth * USD).toFixed(2);
  return val
}
export function convertUSDToETH(usdVal, converter) {
  const { USD = '' } = converter;
  const val = usdVal / USD;
  return val
}
export function getTimeRemaining(endtime) {
  var countDownDate = new Date(endtime).getTime();
  var now = new Date().getTime();
  const total = countDownDate - now;
  const seconds = Math.floor((total / 1000) % 60);
  const minutes = Math.floor((total / 1000 / 60) % 60);
  const hours = Math.floor((total / (1000 * 60 * 60)) % 24);
  const days = Math.floor(total / (1000 * 60 * 60 * 24));

  return {
    total,
    days,
    hours,
    minutes,
    seconds
  };
}

export function formatedDate(date) {
  return date.split("/").reverse().join("-");
}
export function formatedDDMMYYYTOYYYYMMDD(date) {
  var datearray = date.split("/");
  var newdate = datearray[2] + '/' + datearray[1] + '/' + datearray[0];
  return newdate;
}
export function formatDateDDMMYYYY(date) {
  const newDate = moment(date).format('DD/MM/YYYY') === 'Invalid date' ? date : moment(date).format('DD/MM/YYYY')
  return newDate;
}
export function dateISOformate(dateTime) {
  var datearray = dateTime.split("/");
  let yy = datearray[2]
  let mm = datearray[1]
  let dd = datearray[0]
  let time = datearray[3]
  let TimeArray = time.split(":");
  let hh = TimeArray[0]
  let mi = TimeArray[1]
  // console.log('dateTime  yy' + yy)

  // console.log('dateTime  dd' + dd)

  // console.log('dateTime  mm' + mm)

  // console.log('dateTime  hh' + hh)
  // console.log('dateTime  mi' + mi)

  const date = new Date(yy, mm - 1, dd, hh, mi, 0, 0);

  console.log('dateTime  ' + date)
  let newdateTime = date.toISOString();
  console.log('newdateTime  ' + newdateTime)

  return newdateTime;
}
export function timeAgo(date) {
  const newDate = moment(date).format('DD/MM/YYYY') === 'Invalid date' ? date : moment.utc(date).local().startOf('seconds').fromNow()
  return newDate;
}

export function formatDateDDMMYYYYTime(date) {
  const newDate = moment(date).format('DD/MM/YYYY') === 'Invalid date' ? date : moment(date).format('DD/MM/YYYY HH:mm')
  return newDate;
  // return moment(date).format('DD/MM/YYYY hh:mm A');
}
export function formatDateDDMMMYYYY(date) {
  const newDate = moment(date).format('DD/MM/YYYY') === 'Invalid date' ? date : moment(date).format('Do MMM YYYY')
  return newDate;
}