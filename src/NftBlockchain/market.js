import axios from 'axios';
// import Fortmatic from 'fortmatic';
// import Web3Modal from 'web3modal';
// import WalletConnectProvider from '@walletconnect/web3-provider';
// import Web3 from 'web3-react-native';

// import {setRoyalty} from '../redux/account';
// import Web3 from 'web3';
// import WalletConnectProvider from '@walletconnect/web3-provider';

import {executeTxn, sahowToasts, queryEvents, log} from './service';
import {createInstance} from './abiartifacts';
import {upload, convertPriceToEth, awaitTx} from './transaction';
var ethers = require('ethers');
import {useToast} from 'react-native-toast-notifications';

export const auctionContractAddress =
  '0x574E73437807F7b29276F56F0d0C39af0cA184e4';
export const nftmarketaddress = '0x84236809b3BB594CC870d72dA7004F8432914db8';
export const nftaddress = '0x41D3F0070c92375BF21e526c31959a42306d1455';
const rpcEndpoint =
  'https://rinkeby.infura.io/v3/8d4b9c6cf9a942bd9c0468942a96fce0';

// const provider = new WalletConnectProvider({
//   infuraId: '8043bb2cf99347b1bfadfb233c5325c0', // Required
// });

//  Enable session (triggers QR Code modal)

//  Create Web3
// const web3 = new Web3(provider);
// const providerOptions = {
//   walletconnect: {
//     package: WalletConnectProvider,
//     options: {
//       infuraId: '8043bb2cf99347b1bfadfb233c5325c0',
//     },
//   },
//   fortmatic: {
//     package: Fortmatic,
//     options: {
//       key: 'pk_test_391E26A3B43A3350',
//     },
//   },
// };

// const web3Modal = new Web3Modal({
//   network: 'mainnet', // optional
//   cacheProvider: true, // optional
//   providerOptions, // required
// });

// export const getProvider = async () => {
//   try {
//     // let provider = await web3Modal.connect();
//     return provider;
//   } catch (e) {
//     console.log('Could not get a wallet connection', e);
//     return;
//   }
// };
export const getProvider = async () => {
  try {
    let provider = {
      _isSigner: true,
      address: '0x879C7C4dC3188295C019C4C52F50013E28717699',
      provider: {
        _isProvider: true,
        _events: [],
        _emitted: {
          block: -2,
        },
        disableCcipRead: false,
        formatter: {
          formats: {
            transaction: {},
            transactionRequest: {},
            receiptLog: {},
            receipt: {},
            block: {},
            blockWithTransactions: {},
            filter: {},
            filterLog: {},
          },
        },
        anyNetwork: false,
        _network: {
          name: 'homestead',
          chainId: 1,
          ensAddress: '0x00000000000C2E074eC69A0dFb2997BA6C7d2e1e',
        },
        _maxInternalBlockNumber: -1024,
        _lastBlockNumber: -2,
        _maxFilterBlockRange: 10,
        _pollingInterval: 4000,
        _fastQueryDate: 0,
        providerConfigs: [
          {
            provider: {
              _isProvider: true,
              _events: [],
              _emitted: {
                block: -2,
              },
              disableCcipRead: false,
              formatter: {
                formats: {
                  transaction: {},
                  transactionRequest: {},
                  receiptLog: {},
                  receipt: {},
                  block: {},
                  blockWithTransactions: {},
                  filter: {},
                  filterLog: {},
                },
              },
              anyNetwork: false,
              _network: {
                name: 'homestead',
                chainId: 1,
                ensAddress: '0x00000000000C2E074eC69A0dFb2997BA6C7d2e1e',
              },
              _maxInternalBlockNumber: -1024,
              _lastBlockNumber: -2,
              _maxFilterBlockRange: 10,
              _pollingInterval: 4000,
              _fastQueryDate: 0,
              connection: {
                allowGzip: true,
                url: 'https://mainnet.infura.io/v3/84842078b09946638c03157f83405213',
              },
              _nextId: 42,
              apiKey: '84842078b09946638c03157f83405213',
              projectId: '84842078b09946638c03157f83405213',
              projectSecret: null,
            },
            weight: 1,
            stallTimeout: 2000,
            priority: 1,
          },
          {
            provider: {
              _isProvider: true,
              _events: [],
              _emitted: {
                block: -2,
              },
              disableCcipRead: false,
              formatter: {
                formats: {
                  transaction: {},
                  transactionRequest: {},
                  receiptLog: {},
                  receipt: {},
                  block: {},
                  blockWithTransactions: {},
                  filter: {},
                  filterLog: {},
                },
              },
              anyNetwork: false,
              _network: {
                name: 'homestead',
                chainId: 1,
                ensAddress: '0x00000000000C2E074eC69A0dFb2997BA6C7d2e1e',
              },
              _maxInternalBlockNumber: -1024,
              _lastBlockNumber: -2,
              _maxFilterBlockRange: 10,
              _pollingInterval: 4000,
              _fastQueryDate: 0,
              baseUrl: 'https://api.etherscan.io',
              apiKey: '9D13ZE7XSBTJ94N9BNJ2MA33VMAY2YPIRB',
            },
            weight: 1,
            stallTimeout: 2000,
            priority: 1,
          },
          {
            provider: {
              _isProvider: true,
              _events: [],
              _emitted: {
                block: -2,
              },
              disableCcipRead: false,
              formatter: {
                formats: {
                  transaction: {},
                  transactionRequest: {},
                  receiptLog: {},
                  receipt: {},
                  block: {},
                  blockWithTransactions: {},
                  filter: {},
                  filterLog: {},
                },
              },
              anyNetwork: false,
              _network: {
                name: 'homestead',
                chainId: 1,
                ensAddress: '0x00000000000C2E074eC69A0dFb2997BA6C7d2e1e',
              },
              _maxInternalBlockNumber: -1024,
              _lastBlockNumber: -2,
              _maxFilterBlockRange: 10,
              _pollingInterval: 4000,
              _fastQueryDate: 0,
              connection: {
                allowGzip: true,
                url: 'https://eth-mainnet.alchemyapi.io/v2/_gg7wSSi0KMBsdKnGVfHDueq6xMB9EkC',
              },
              _nextId: 42,
              apiKey: '_gg7wSSi0KMBsdKnGVfHDueq6xMB9EkC',
            },
            weight: 1,
            stallTimeout: 2000,
            priority: 1,
          },
          {
            provider: {
              _isProvider: true,
              _events: [],
              _emitted: {
                block: -2,
              },
              disableCcipRead: false,
              formatter: {
                formats: {
                  transaction: {},
                  transactionRequest: {},
                  receiptLog: {},
                  receipt: {},
                  block: {},
                  blockWithTransactions: {},
                  filter: {},
                  filterLog: {},
                },
              },
              anyNetwork: false,
              _network: {
                name: 'homestead',
                chainId: 1,
                ensAddress: '0x00000000000C2E074eC69A0dFb2997BA6C7d2e1e',
              },
              _maxInternalBlockNumber: -1024,
              _lastBlockNumber: -2,
              _maxFilterBlockRange: 10,
              _pollingInterval: 4000,
              _fastQueryDate: 0,
              connection: {
                url: 'https://eth-mainnet.gateway.pokt.network/v1/lb/6004bcd10040261633ade990',
                headers: {},
              },
              _nextId: 42,
              applicationId: '6004bcd10040261633ade990',
              loadBalancer: true,
              applicationSecretKey: null,
            },
            weight: 1,
            stallTimeout: 2000,
            priority: 1,
          },
          {
            provider: {
              _isProvider: true,
              _events: [],
              _emitted: {
                block: -2,
              },
              disableCcipRead: false,
              formatter: {
                formats: {
                  transaction: {},
                  transactionRequest: {},
                  receiptLog: {},
                  receipt: {},
                  block: {},
                  blockWithTransactions: {},
                  filter: {},
                  filterLog: {},
                },
              },
              anyNetwork: false,
              _network: {
                name: 'homestead',
                chainId: 1,
                ensAddress: '0x00000000000C2E074eC69A0dFb2997BA6C7d2e1e',
              },
              _maxInternalBlockNumber: -1024,
              _lastBlockNumber: -2,
              _maxFilterBlockRange: 10,
              _pollingInterval: 4000,
              _fastQueryDate: 0,
              connection: {
                url: 'https://cloudflare-eth.com/',
              },
              _nextId: 42,
            },
            weight: 1,
            stallTimeout: 750,
            priority: 1,
          },
          {
            provider: {
              _isProvider: true,
              _events: [],
              _emitted: {
                block: -2,
              },
              disableCcipRead: false,
              formatter: {
                formats: {
                  transaction: {},
                  transactionRequest: {},
                  receiptLog: {},
                  receipt: {},
                  block: {},
                  blockWithTransactions: {},
                  filter: {},
                  filterLog: {},
                },
              },
              anyNetwork: false,
              _network: {
                name: 'homestead',
                chainId: 1,
                ensAddress: '0x00000000000C2E074eC69A0dFb2997BA6C7d2e1e',
              },
              _maxInternalBlockNumber: -1024,
              _lastBlockNumber: -2,
              _maxFilterBlockRange: 10,
              _pollingInterval: 4000,
              _fastQueryDate: 0,
              connection: {
                allowGzip: true,
                url: 'https://rpc.ankr.com/eth/9f7d929b018cdffb338517efa06f58359e86ff1ffd350bc889738523659e7972',
              },
              _nextId: 42,
              apiKey:
                '9f7d929b018cdffb338517efa06f58359e86ff1ffd350bc889738523659e7972',
            },
            weight: 1,
            stallTimeout: 2000,
            priority: 1,
          },
        ],
        quorum: 2,
        _highestBlockNumber: -1,
      },
    };
    return provider;
  } catch (e) {
    console.log('Could not get a wallet connection', e);
    return;
  }
};
export const getAuctionContract = async getPro => {
  const data = await axios.get(
    'https://nft.statledger.io/artifacts/contracts/AuctionContract.sol/AuctionContract.json',
  );
  console.log('Data Value of AuctionContract is', data.data);
  const provider = new ethers.providers.Web3Provider(getPro);
  const signer = provider.getSigner();
  let auctionContract = new ethers.Contract(
    auctionContractAddress,
    data.data.abi,
    signer,
  );
  return auctionContract;
};
export const resaleNFT = async (_price, _itemid, provider) => {
  const market = createInstance(provider, 'Market');
  const nft = createInstance(provider, 'NFT');
  let _nftaddress = nft.address;

  const price = await convertPriceToEth(_price);
  log('putforsale', 'Price Value', price);

  let response = await executeTxn(market, provider, 'placeItemForReSale', [
    _nftaddress,
    _itemid,
    price,
  ]);
  log(
    'placeItemForReSale',
    'Hash value ',
    response.txHash,
    process.env.REACT_APP_EXPLORER,
  );
  const returnvalue = await queryEvents(
    market,
    provider,
    'itemSetForResale',
    response.blockNumber,
  );
  log('placeItemForReSale', 'MarketItemCreated Event', returnvalue);
  showToasts(response.txHash);
};
// export const verifyAuth = async () => {
//   await window.web3.currentProvider.enable();
//   const web3 = new Web3(window.web3.currentProvider);
//   const message = 'An amazing message, for use with MetaMask!';
//   const signatures = await web3.eth.personal.sign(
//     message,
//     '0x067245aaC3731eC9d84579090eAAB4FF12AAF8A1',
//     '',
//   );
//   console.log('sigs', signatures);
// };

// verifyAuth()
export const getMarketContract = async getPro => {
  try {
    const data = await axios.get(
      'https://nft.statledger.io/artifacts/contracts/StatMarketV4.sol/NFTMIO.json',
    );
    // console.log("Data Value of Market is", data.data);
    const provider = new ethers.providers.Web3Provider(getPro);
    const signer = provider.getSigner();
    let marketContract = new ethers.Contract(
      nftmarketaddress,
      data.data.abi,
      signer,
    );
    return marketContract;
  } catch (err) {
    console.log(err);
  }
};

export const getNFTContract = async getPro => {
  const data = await axios.get(
    'https://nft.statledger.io/artifacts/contracts/NFT.sol/NFT721.json',
  );
  const provider = new ethers.providers.Web3Provider(getPro);
  const signer = provider.getSigner();
  let nftcontract = new ethers.Contract(nftaddress, data.data.abi, signer);
  return nftcontract;
};

const getDynamicNFT = async (nftaddr, getPro) => {
  const data = await axios.get(
    'https://nft.statledger.io/artifacts/contracts/NFT.sol/NFT721.json',
  );
  // console.log("Dynamic Data Value of NFT is", data.data);
  const provider = new ethers.providers.Web3Provider(getPro);
  const signer = provider.getSigner();
  let nftcontract = new ethers.Contract(nftaddr, data.data.abi, signer);
  return nftcontract;
};

const getDynamicNFTEarly = async nftaddr => {
  const nftdata = await axios.get(
    'https://nft.statledger.io/artifacts/contracts/NFT.sol/NFT721.json',
  );
  console.log('nftdata early', nftdata.data);
  const provider = new ethers.providers.JsonRpcProvider(rpcEndpoint);
  const nftcontract = new ethers.Contract(nftaddr, nftdata.data.abi, provider);
  return nftcontract;
};

const convertPricefromEth = async n => {
  const convertedprice = ethers.utils.formatUnits(n, 'ether');
  return convertedprice;
};
export const fetchMarketItems = async (
  marketContract,
  getPro,
  walletAddress,
) => {
  const data = await marketContract.fetchMarketItems();
  console.log('fetchMarketItems', data);
  const auctioncon = await getAuctionContract(getPro);
  const items = await Promise.all(
    data.map(async i => {
      console.log('I value of fetchMarkeItem is', i);
      let auctiondetails = {};
      if (i.onAuction) {
        const aucdata = await auctioncon.getAuctionbyTokenId(
          i.tokenId.toNumber(),
        );
        console.log('aucdata is', aucdata);
        let auctionHighestPrice = await convertPricefromEth(
          aucdata.highestBid.toString(),
          'ether',
        );
        var starttimeconverted = new Date(aucdata.startTime.toString() * 1000);
        var endtimeconverted = new Date(aucdata.endTime.toString() * 1000);
        auctiondetails = {
          auctionid: aucdata.auctionid.toNumber(),
          startTime: starttimeconverted,
          endTime: endtimeconverted,
          highestBid: auctionHighestPrice,
          highestBidder: aucdata.highestBidder,
          auctionclosed: aucdata.auctionclosed,
        };
      }
      var nftaddr = i.nftContract;
      var nftContract = await getDynamicNFT(nftaddr, getPro);
      if (i.tokenId.toNumber() > 0) {
        const tokenUri = await nftContract.tokenURI(i.tokenId);
        const meta = await axios.get(tokenUri);
        let price = await convertPricefromEth(i.price.toString(), 'ether');
        let item = {
          price,
          itemId: i.itemId.toNumber(),
          seller: i.seller,
          owner: i.owner,
          metavalue: meta,
          image: meta.data.image,
          name: meta.data.name,
          description: meta.data.description,
          auctiondetails,
        };
        return item;
      }
    }),
  );
  console.log('Market Items are', items);
  return items.filter(
    item =>
      item && item.seller !== walletAddress && !item.auctiondetails.auctionid,
  );
};

export const fetchEarlyMarketItems = async () => {
  const nftmio = await axios.get(
    'https://nft.statledger.io/artifacts/contracts/StatMarketV4.sol/NFTMIO.json',
  );
  console.log('nftmio Value of Market is', nftmio.data);
  const provider = new ethers.providers.JsonRpcProvider(rpcEndpoint);
  const marketContract = new ethers.Contract(
    nftmarketaddress,
    nftmio.data.abi,
    provider,
  );
  const data = await marketContract.fetchMarketItems();
  const items = await Promise.all(
    data.map(async i => {
      var nftaddr = i.nftContract;
      var nftContract = await getDynamicNFTEarly(nftaddr);
      const tokenUri = await nftContract.tokenURI(i.tokenId);
      const meta = await axios.get(tokenUri);
      let price = ethers.utils.formatUnits(i.price.toString(), 'ether');
      let item = {
        price,
        itemId: i.itemId.toNumber(),
        seller: i.seller,
        owner: i.owner,
        image: meta.data.image,
        name: meta.data.name,
        description: meta.data.description,
        metavalue: meta,
      };
      return item;
    }),
  );
  console.log('Items are ', items);
  return items;
};

export const buyNftold = async (
  marketcontract,
  _price,
  _itemid,
  nftaddr,
  dispatch,
) => {
  let returnData = {};
  const price = await convertPriceToEth(_price);
  console.log(marketcontract, _price, _itemid, nftaddr);
  let transaction = await marketcontract.buyNFT(nftaddr, _itemid, {
    value: price,
  });
  await transaction.wait();
  console.log('Blockchain buy transaction - ', transaction);
  returnData.transactionHash = transaction.hash;
  return returnData;
};

// export const buyNft = async (_price, _itemid, provider) => {
//   //input parameters
//   console.log(_price);
//   const market = createInstance(provider, 'Market');
//   const platform = createInstance(provider, 'Platform');
//   const nft = createInstance(provider, 'NFT');
//   const price = await convertPriceToEth(_price);
//   log('buyNFT', 'Price Value', price);
//   let response1 = await executeTxn(platform, provider, 'approve', [
//     market.address,
//     price,
//   ]);
//   log('buyNFT', 'approve hash', response1.txHash);
//   let transaction = await provider.getTransaction(response1.txHash);
//   const web3 = new Web3(window.web3.currentProvider);
//   let tran2 = await awaitTx(web3, response1.txHash, {});
//   console.log('minted is ', tran2);
//   console.log('transaction details', transaction);
//   showToasts(response1.txHash);
//   if (response1.txHash) {
//     let response2 = await executeTxn(market, provider, 'buyNFT', [
//       nft.address,
//       _itemid,
//       1,
//     ]);
//     console.log(
//       'buyNFT',
//       'Hash value',
//       response2.txHash,
//       process.env.REACT_APP_EXPLORER,
//     );
//     showToasts(response2.txHash);
//     return tran2;
//   }
// };

export const mintToken = async (metaurl, provider) => {
  try {
    // const data = JSON.stringify(Data);

    // const metaurl = await upload(data, 'createNFT Metadata');
    console.log('mintToken metaurl ' + metaurl);

    const nft = createInstance(provider, 'NFT');
    // console.log('nft Response');

    const response = await executeTxn(nft, provider, 'createToken', [
      metaurl,
      false,
      false,
    ]);
    // console.log('executeTxn Response');

    // console.log('mintNFT', 'Response', response);
    // console.log('mintNFT', 'Hash Value', response.txHash);
    // use toNumber() to convert from bigNumber to normal numeric
    const returnvalue = await queryEvents(
      nft,
      provider,
      'TokenGenerated',
      response.blockNumber,
    );
    console.log('mintNFT', 'ReturnedValue', returnvalue);

    const returnData = {
      transactionHash: response.txHash,
      tokenId: returnvalue.tokenId.toNumber(),
      transactionPayload: response,
    };

    return returnData;
  } catch (err) {
    console.log(err);
    // toast(err.message || err, {type: 'error'});
  }
};

export const sellNftold = async (
  marketContract,
  _collectionname,
  tokenId,
  _price,
  _nftaddress,
  _tokenno,
  _royalty,
  _category,
  endTime,
  pro,
  resale,
  _usercollectionId,
) => {
  console.log(
    _collectionname,
    tokenId,
    _price,
    _nftaddress,
    _tokenno,
    _royalty,
    _category,
    endTime,
    pro,
    resale,
  );
  let collectionId;
  let addToMasterCollection = false;
  let addToUserCollection = false;
  const price = ethers.utils.parseUnits(_price, 'ether');
  const price2 = await convertPriceToEth(_price);

  let returnData = {};

  console.log(_collectionname, _usercollectionId);
  if (_collectionname == null && _usercollectionId == null) {
    addToMasterCollection = true;
    collectionId = 1;
    console.log('Iam here in masterCollection', collectionId);
  } else if (_collectionname !== null) {
    console.log('Iam here in collection', _collectionname);
    let transaction1 = await marketContract.createCollection(
      _collectionname,
      _tokenno,
      nftaddress,
      price,
      _category,
      false,
    );
    let tx1 = await transaction1.wait();
    let event1 = tx1.events[0];
    let value1 = event1.args[2];
    collectionId = value1.toNumber();
    console.log('Iam here in create userCollection', collectionId);
  } else if (_usercollectionId !== null) {
    addToUserCollection = true;
    collectionId = _usercollectionId;
    console.log('Iam here in existing userCollection', _usercollectionId);
  }
  let isauction;
  if (endTime === null) {
    isauction = false;
  } else {
    isauction = true;
  }
  console.log('price2 is ', price2);
  let transaction;
  if (resale) {
    console.log('in resale ', nftaddress, tokenId, price2);
    transaction = await marketContract.placeItemForReSale(
      nftaddress,
      _tokenno,
      price2,
    );
    await transaction.wait();
    returnData.transactionHash = transaction.hash;
  } else {
    transaction = await marketContract.placeItemForSale(
      nftaddress,
      _tokenno,
      price,
      collectionId,
      _royalty,
      _category,
      isauction,
      addToMasterCollection,
      addToUserCollection,
    );
    let tx = transaction.wait();
    console.log('Transaction - ', tx, transaction);
    returnData.transactionHash = transaction.hash;
  }

  if (isauction) {
    let auctionContract = await getAuctionContract(pro);
    let transaction1 = await auctionContract.createAuction(
      _tokenno,
      price,
      endTime ? 1 : null,
      endTime,
      _nftaddress,
    );
    await transaction1.wait();
    returnData.transactionHash = transaction1.hash;
    console.log('Transaction auction - ', transaction1);
  }

  return returnData;
};

export const sellNft = async (
  provider,
  _price,
  _starttimeauction,
  _collectionname,
  _tokenno,
  _royalty,
  _category,
  _endtimeauction,
  _qty,
  resale,
  _usercollectionId,
  currency,
) => {
  try {
    console.log(currency);
    if (resale) {
      let res = await resaleNFT(_price, _tokenno, provider);
      return res;
    }
    const market = createInstance(provider, 'Market');
    const auction = createInstance(provider, 'Auction');
    const platform = createInstance(provider, currency);
    const nft = createInstance(provider, 'NFT');
    const nft1155 = createInstance(provider, 'NFT1155');

    let _nftaddress = nft.address;
    const _currency = platform.address;
    let _currencyType;
    const price = await convertPriceToEth(_price);
    console.log('putforsale', 'Price Value', price);

    let isauction;
    if (_starttimeauction == null) {
      isauction = false;
    } else {
      isauction = true;
    }
    if (_qty === 1) {
      _nftaddress = nft.address;
      _currencyType = 0;
    } else {
      _nftaddress = nft1155.address;
      _currencyType = 1;
    }
    console.log(
      _nftaddress,
      _tokenno,
      price,
      _royalty,
      _category,
      isauction,
      _currency,
      _qty,
      _currencyType,
    );
    let response = await executeTxn(market, provider, 'placeItemForSale', [
      _nftaddress,
      _tokenno,
      price,
      _royalty,
      _category,
      isauction,
      _currency,
      _qty,
      _currencyType,
    ]);
    log(
      'placeItemForSale',
      'Hash value ',
      response.txHash,
      process.env.REACT_APP_EXPLORER,
    );
    const returnvalue = await queryEvents(
      market,
      provider,
      'MarketItemCreated',
      response.blockNumber,
    );
    log('placeItemForSale', 'MarketItemCreated Event', returnvalue);
    showToasts(response.txHash);

    let returnData = {
      nftId: returnvalue.itemId.toNumber(),
      sellPayload: response,
    };

    if (isauction) {
      let response = await executeTxn(auction, provider, 'createAuction', [
        _tokenno,
        price,
        _starttimeauction,
        _endtimeauction,
        _nftaddress,
        _currency,
      ]);
      console.log('Create auction - ', response);
      log(
        'createAuction',
        'Hash value',
        response.txHash,
        process.env.REACT_APP_EXPLORER,
      );
      const returnvalue = await queryEvents(
        auction,
        provider,
        'AuctionCreated',
        response.blockNumber,
      );
      console.log('Auction created - ', returnvalue);
      log('createAuction', 'AuctionCreated Event', returnvalue);
      showToasts(response.txHash);

      returnData.auctionId = returnvalue._acutionid.toNumber();
      returnData.auctionPayload = response;
    }

    return returnData;
  } catch (err) {
    console.log('Error in sell - ', err);
    throw err;
  }
};

export const fetchUserPurchased = async (marketContract, getPro) => {
  const data = await marketContract.fetchMyNFTs();
  const items = await Promise.all(
    data.map(async i => {
      console.log(i);
      var nftaddr = i.nftContract;
      var nftContract = await getDynamicNFT(nftaddr, getPro);
      const tokenUri = await nftContract.tokenURI(i.tokenId);
      const meta = await axios.get(tokenUri);
      let price = ethers.utils.formatUnits(i.price.toString(), 'ether');
      let item = {
        price,
        itemId: i.itemId.toNumber(),
        seller: i.seller,
        owner: i.owner,
        image: meta.data.image,
        name: meta.data.name || meta.data._assetname,
        description: meta.data.description,
        resale: true,
        collectionId: i.associatedCollectionId.toNumber(),
      };
      return item;
    }),
  );
  console.log('My NFTS', items);
  return items;
};

export const fetchUserCreated = async (marketContract, getPro) => {
  const data = await marketContract.fetchItemsCreated();
  const items = await Promise.all(
    data.map(async i => {
      var nftaddr = i.nftContract;
      var nftContract = await getDynamicNFT(nftaddr, getPro);
      const tokenUri = await nftContract.tokenURI(i.tokenId);
      // console.log("TokenURI value is", tokenUri);
      const meta = await axios.get(tokenUri);
      let price = ethers.utils.formatUnits(i.price.toString(), 'ether');
      let item = {
        price,
        itemId: i.itemId.toNumber(),
        seller: i.seller,
        owner: i.owner,
        image: meta.data.image,
        name: meta.data.name,
        description: meta.data.description,
        metavalue: meta,
      };
      return item;
    }),
  );
  return items;
};

export const fetchMyRoyalty = async dispatch => {
  const provider = await getProvider();
  const marketContract = await getMarketContract(provider);
  const data = await marketContract.fetchMyRoyalty();
  let balance = ethers.utils.formatUnits(data.toString(), 'ether');
  // dispatch(setRoyalty(balance));
};

export const fetchUnlistedItems = async (nftContract, wallet) => {
  //fetchUserCreatedFromTokenContract
  const data = await nftContract.walletOfOwner(wallet);
  const getPro = await getProvider();
  const marketContract = await getMarketContract(getPro);
  const marketdata = await marketContract.fetchMarketItemsHistory();
  console.log('fetchUserCreatedFromTokenContract nftdata', data);
  console.log('fetchUserCreatedFromTokenContract marketdata', marketdata);

  const marketTokenArray = [];
  await Promise.all(
    marketdata.map(async mk => {
      marketTokenArray.push(mk.tokenId.toNumber());
      // console.log("marketTokenArray", marketTokenArray)
    }),
  );

  const items = await Promise.all(
    data.map(async i => {
      console.log(
        'I value is',
        i.toNumber(),
        marketTokenArray.indexOf(i.toNumber()),
      );
      if (marketTokenArray.indexOf(i.toNumber()) === -1) {
        const tokenUri = await nftContract.tokenURI(i.toNumber());
        console.log('TokenURI value is', tokenUri);
        const meta = await axios.get(tokenUri);
        let item = {
          tokenId: i.toNumber(),
          nftaddress: nftaddress,
          metavalue: meta.data,
          ...meta.data,
        };
        // console.log("Token Created are", item)
        return item;
      }
    }),
  );
  // console.log("Token created by user is", items)
  return items.filter(item => item);
};

export const fetchAuctionItems = async () => {
  const auctiondata = await axios.get(
    'https://nft.statledger.io/artifacts/contracts/AuctionContract.sol/AuctionContract.json',
  );
  console.log('auctiondata early', auctiondata.data);
  const provider = new ethers.providers.JsonRpcProvider(rpcEndpoint);
  const auction = new ethers.Contract(
    auctionContractAddress,
    auctiondata.data.abi,
    provider,
  );
  const data = await auction.getAuctions();
  console.log('Auction Array  is', data);
  const items = await Promise.all(
    data.map(async i => {
      var nftaddr = i.nftaddress;
      var nftContract = await getDynamicNFTEarly(nftaddr);
      const tokenUri = await nftContract.tokenURI(i.tokenId);
      const meta = await axios.get(tokenUri);
      console.log('metavalue is', meta);
      let price = await convertPricefromEth(i.highestBid.toString(), 'ether');
      let price2 = await convertPricefromEth(i.price.toString(), 'ether');
      let item = {
        tokenid: i.tokenId.toNumber(),
        nftaddress: i.nftaddress,
        seller: i.seller,
        startTime: i.startTime,
        endTime: i.endTime,
        highestBid: parseFloat(price) > parseFloat(price2) ? price : price2,
        highestBidder: i.highestBidder,
        image: meta.data.image,
        name: meta.data.name,
        description: meta.data.description,
        metavalue: meta,
        auctionId: i.auctionid.toNumber(),
      };
      return item;
    }),
  );
  console.log('Auction Items are ', items);
  return items;
};

export const placeBidold = async (_auctionidbid, _pricebid) => {
  let pro = await getProvider();
  let auccon = await getAuctionContract(pro);
  const price = ethers.utils.parseUnits(_pricebid, 'ether');
  console.log('priceBid is', price);
  let transaction = await auccon.bid(_auctionidbid, {value: price});
  await transaction.wait();
  return transaction;
};

// export const placeBid = async (provider, _auctionid, _price) => {
//   //input parameters
//   const price = await convertPriceToEth(_price);
//   log('placeMyBid', 'Price Value', price);
//   // //approve auction contract to transfer the token
//   const auction = createInstance(provider, 'Auction');
//   const platform = createInstance(provider, 'Platform');

//   let response1 = await executeTxn(platform, provider, 'approve', [
//     auction.address,
//     price,
//   ]);
//   log('placeMyBid', 'Approve Hash', response1.txHash);
//   let transaction = await provider.getTransaction(response1.txHash);
//   const web3 = new Web3(window.web3.currentProvider);
//   let tran2 = await awaitTx(web3, response1.txHash, {});
//   console.log('transaction is: ', transaction);
//   console.log('minted is :', tran2);
//   showToasts(response1.txHash);
//   if (response1.txHash) {
//     let response2 = await executeTxn(auction, provider, 'bid', [
//       _auctionid,
//       price,
//     ]);
//     log('placeMyBid', 'After Bid Hash', response2.txHash);
//     showToasts(response2.txHash);
//     return {hash: response2.txHash, payload: tran2};
//   }
// };

export const FinalizeBidold = async auctionId => {
  let pro = await getProvider();
  let auccon = await getAuctionContract(pro);
  let transaction = await auccon.finalize(auctionId);
  await transaction.wait();
  return transaction;
};
export const FinalizeBid = async (_auctionid, provider) => {
  //input parameters
  const auction = createInstance(provider, 'Auction');
  let response = await executeTxn(auction, provider, 'finalize', [_auctionid]);
  log(
    'FinalizeBid',
    'Hash Value',
    response.txHash,
    process.env.REACT_APP_EXPLORER,
  );
  showToasts(response.txHash);
};

export const createMasterCollection = async (
  marketcontract,
  masterCollectionName,
) => {
  let masterPrice = '0';
  const price = await convertPriceToEth(masterPrice);
  let transaction1 = await marketcontract.createCollection(
    masterCollectionName,
    0,
    nftaddress,
    price,
  );
  let tx1 = await transaction1.wait();
  let event1 = tx1.events[0];
  let value1 = event1.args[2];
  let collectionId = value1.toNumber();
  console.log('MasterCollection ID is', collectionId);
};

export const bringEarlyMarketCollections = async () => {
  const getPro = await getProvider();
  const marketContract = await getMarketContract(getPro);
  const data = await marketContract.fetchMarketCollections();
  console.log('bringEarlyMarketCollections data beginning ', data);
  if (data.length !== 0) {
    let totArray = [];
    await Promise.all(
      data.map(async i => {
        var nftaddr = i.nftContract;
        const tokenArray = [];
        const ownerArray = [];
        const itemIdArray = [];
        const priceArray = [];
        const categoryArray = [];
        i.tokenIds.map(async (tok, index) => {
          let tokprice = await convertPricefromEth(
            i.prices[index].toString(),
            'ether',
          );
          priceArray.push(tokprice);
          tokenArray.push(i.tokenIds[index].toNumber());
          itemIdArray.push(i.itemIds[index].toNumber());
          categoryArray.push(i.categoriesList[index]);
          ownerArray.push(i.minterList[index]);
        });

        let item = {
          itemId: itemIdArray,
          collectionId: i.collectionId.toNumber(),
          tokenId: tokenArray,
          collectionName: i.collectionName,
          owner: i.owner,
          tokenOwnerList: ownerArray,
          price: priceArray,
          category: categoryArray,
          nftaddress: nftaddr,
          path: '/collection/fetchById?id=' + i.collectionId.toNumber(),
        };
        totArray.push(item);
      }),
    );
    console.log('bringEarlyMarketCollections data Ending  ', totArray);
    return totArray;
  }
};

export const explore = async () => {
  const getPro = await getProvider();
  const marketContract = await getMarketContract(getPro);
  const data = await marketContract.exploreItems();
  const items = await Promise.all(
    data.map(async i => {
      var nftaddr = i.nftContract;
      var nftContract = await getDynamicNFT(nftaddr, getPro);
      const tokenUri = await nftContract.tokenURI(i.tokenId);
      const meta = await axios.get(tokenUri);
      let item = {
        itemId: i.itemId.toNumber(),
        owner: i.owner,
        category: i.category,
        nftContract: i.nftContract,
        image: meta.data.image,
        name: meta.data._assetname,
        description: meta.data._description,
        metadata: meta.data,
      };
      return item;
    }),
  );
  console.log('item is', items);
  return items;
};

export const fetchMarketCollectionById = async (
  _collectionIdFetch,
  walletAddress,
) => {
  const getPro = await getProvider();
  const marketContract = await getMarketContract(getPro);
  const nftContract = await getNFTContract(getPro);

  const data = await marketContract.fetchCollectionById(_collectionIdFetch);
  console.log('fetchCollectionById data is', data, data.tokenIds);
  let totArray = [];
  await Promise.all(
    data.tokenIds.map(async (tok, index) => {
      let tokprice = await convertPricefromEth(
        data.prices[index].toString(),
        'ether',
      );
      if (tok.toNumber() > 0) {
        const tokenUri = await nftContract.tokenURI(tok);
        const meta = await axios.get(tokenUri);
        let item = {
          collectionName: data.collectionName,
          owner: data.minterList[index],
          price: tokprice,
          tokenId: data.itemIds[index].toNumber(),
          category: data.categoriesList[index],
          metavalue: meta.data,
          image: meta.data.image,
        };
        totArray.push(item);
      }
    }),
  );
  console.log('totArray', totArray);
  return totArray.filter(
    item =>
      item &&
      item.seller !== walletAddress &&
      item.owner !== walletAddress &&
      !item.auctiondetails?.auctionid,
  );
};
