import { ethers } from 'ethers';

import {
  LazyContract as lazyaddress,
  AuctionContract as auctionaddress,
  MarketPlace as marketaddress,
  NFT721 as nftaddress,
  Platform as tokenaddress,
  NFT1155 as nft1155address,
  USDT as usdtaddress,
  USDC as usdcaddress,
  DAI as daiaddress
} from './deploy.json';
import { abi as auctionabi } from '../artifacts/contracts/AuctionContract.sol/AuctionContract.json';
import { abi as lazyabi } from '../artifacts/contracts/LazyFactory.sol/LazyFactory.json';
import { abi as marketabi } from '../artifacts/contracts/Marketplace.sol/Marketplace.json';
import { abi as nftabi } from '../artifacts/contracts/NFT.sol/NFT721.json';
import { abi as platformabi } from '../artifacts/contracts/PlatformToken.sol/PlatformToken.json';
import { abi as nft1155abi } from '../artifacts/contracts/NFT1155.sol/NFT1155.json';
import { abi as usdtabi } from '../currencyabi/usdt.json';
import { abi as usdcabi } from '../currencyabi/usdc.json';
import { abi as daiabi } from '../currencyabi/dai.json';

export function createInstance(provider, name) {
  if (name === "Lazy") {
    return new ethers.Contract(lazyaddress, lazyabi, provider);
  }
  if (name === "Auction") {
    return new ethers.Contract(auctionaddress, auctionabi, provider);
  }
  if (name === "Market") {
    return new ethers.Contract(marketaddress, marketabi, provider);
  }
  if (name === "NFT") {
    return new ethers.Contract(nftaddress, nftabi, provider);
  }
  if (name === "Platform") {
    console.log('here')
    return new ethers.Contract(tokenaddress, platformabi, provider);
  }
  if (name === "USDT") {
    return new ethers.Contract(usdtaddress, usdtabi, provider);
  }
  if (name === "USDC") {
    return new ethers.Contract(usdcaddress, usdcabi, provider);
  }
  if (name === "NFT1155") {
    return new ethers.Contract(nft1155address, nft1155abi, provider);
  }
  if (name === "DAI") {
    return new ethers.Contract(daiaddress, daiabi, provider);
  }
}
