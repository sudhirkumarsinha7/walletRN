import { createInstance } from './forwarder';
import { signMetaTxRequest } from './signer';
import { ethers } from 'ethers';
import axios from 'axios'
import { log,queryData } from './service'
import {DEV_URL} from '../config/config'

export async function sendTx(contract, functionname, input, provider) {
  log("sendTx", "Sending tx to set", input);  const feeData = await provider.getFeeData();
  const dataSize = Buffer.from(input).length;
  const gasCalc = (feeData.gasPrice + (feeData.maxFeePerGas - feeData.maxPriorityFeePerGas) * dataSize);
  const gasLimit = gasCalc < feeData.maxFeePerGas ? parseInt(gasCalc/100000) : 1000000
  const tx = await contract[functionname](...input,{ gasLimit: gasLimit});
  //const tx = await contract[functionname](...input);
  const txresponse = await tx.wait();
  const blkno = txresponse.blockNumber ? txresponse.blockNumber : 0;
  const txnevnts = txresponse.events ? txresponse.events : "";
  let obj = { txHash: txresponse.transactionHash, blockNumber: blkno, event: txnevnts }
  log("sendTx", "Responses", obj);
  return obj;
}

export async function queryReports(contract, functionname, input) {
  const result = await contract[functionname](...input);
  return result;
}

export async function qryEvents(contract, eventname, blockNumber) {
  log("qryEvents", "inputs are", contract, eventname, blockNumber);
  const events = await contract.queryFilter(contract.filters[`${eventname}`](), blockNumber, blockNumber);
  log("qryEvents", "event value", events, events[0].args)
  return events[0].args;
}

export async function sendMetaTx(contract, provider, signer, functionname, input) {
  log("sendMetaTx", "Sending meta-tx to set", input);

  let url = DEV_URL;
  // if (process.env.REACT_APP_DEBUGMODE === "INTEGRATION") {
  //   url = process.env.REACT_APP_WEBHOOK_URL_INTEGRATION;
  // } else {
  //   url = process.env.REACT_APP_WEBHOOK_URL_TEST;
  // }
  log("sendMetaTx", "Defender URL", url);

  if (!url) throw new Error(`Missing relayer url`);
  const forwarder = createInstance(provider);
  const from = await signer.getAddress();
  const data = contract.interface.encodeFunctionData(functionname, [...input]);
  const to = contract.address;

  const request = await signMetaTxRequest(signer.provider, forwarder, { to, from, data });
  const responses = await axios.post(url, request)
  console.log("RESPONSE OF META TXN",responses)
  log("sendMetaTx","responses",responses)
  const finalresult = JSON.parse(responses.data.result);
  console.log("FINALY RESPONE OF META TXN",finalresult)
  let obj = { txHash: finalresult.txHash, blockNumber: finalresult.blockNumber, event: "" }
  log("sendMetaTx", "Responses", obj);
  return obj;
}

export async function signAndBalance(provider) {
  // if (!window.ethereum) throw new Error(`User wallet not found`);
  // await window.ethereum.enable();
  const userProvider = provider
  const userNetwork = await userProvider.getNetwork();
  log("signAndBalance","userNetwork",userNetwork)
  // if (userNetwork.chainId !== 80001) throw new Error(`Please switch to Mumbai for signing`);
  const signer = userProvider.getSigner();
  const from = await signer.getAddress();
  const balance = await provider.getBalance(from);
  return { balance, signer };
};

export async function convertPriceToEth(n) {
  const convertedprice = ethers.utils.parseUnits(n, 'ether');
  return convertedprice;
}

export async function convertPricefromEth(n) {
  const convertedprice = ethers.utils.formatUnits(n, 'ether');
  return convertedprice;
}
export async function checkCurrencyBalanceForUser(token, provider,account,actualprice) {
  let data = await queryData(token, provider, 'balanceOf', [account]);
  let actualBalance = await convertPricefromEth(data);
  log("checkUserBalance","actualBalance", actualBalance, actualprice)
  console.log(actualBalance, actualprice)
  if ((actualBalance === 0) || (parseFloat(parseFloat(actualBalance)) < parseFloat(parseFloat(actualprice)))) {
    log("buyNFT", "Balance of required currency is either 0 or lower than expected price", actualBalance);
    return false;
  }else{
    return true;
  }
}
export async function checkCurrencyBalanceForToken(token, provider,account) {
  let data = await queryData(token, provider, 'balanceOf', [account]);
  let actualBalance = await convertPricefromEth(data);
  return actualBalance;
}
export async function upload(n, from, token, baseURL) {
  try {
    let config = {
      headers: {
        Authorization: `Bearer ${token}`
      }
    }
    const res = await axios.post(`${baseURL}/api/image_service/ipfs/uploadToIpfs`, n, config)
    const url = `https://ipfs.infura.io/ipfs/${res.data.data.ipfsRes}`
    log("IPFS", `Upload initiated for ${from}`, url)
    return url;
  } catch (error) {
    log('IPFS', 'Error', error)
  }
}
const DEFAULT_INTERVAL = 500;
const DEFAULT_BLOCKS_TO_WAIT = 0;

export function awaitTx(web3, txnHash, options) {
  var interval =
    options && options.interval ? options.interval : DEFAULT_INTERVAL;
  var blocksToWait =
    options && options.blocksToWait
      ? options.blocksToWait
      : DEFAULT_BLOCKS_TO_WAIT;
  var transactionReceiptAsync = async function (txnHash, resolve, reject) {
    try {
      var receipt = web3.eth.getTransactionReceipt(txnHash);
      if (!receipt) {
        setTimeout(function () {
          transactionReceiptAsync(txnHash, resolve, reject);
        }, interval);
      } else {
        if (blocksToWait > 0) {
          var resolvedReceipt = await receipt;
          if (!resolvedReceipt || !resolvedReceipt.blockNumber)
            setTimeout(function () {
              transactionReceiptAsync(txnHash, resolve, reject);
            }, interval);
          else {
            try {
              var block = await web3.eth.getBlock(resolvedReceipt.blockNumber);
              var current = await web3.eth.getBlock("latest");
              if (current.number - block.number >= blocksToWait) {
                var txn = await web3.eth.getTransaction(txnHash);
                if (txn.blockNumber != null) resolve(resolvedReceipt);
                else
                  reject(
                    new Error(
                      "Transaction with hash: " +
                        txnHash +
                        " ended up in an uncle block."
                    )
                  );
              } else
                setTimeout(function () {
                  transactionReceiptAsync(txnHash, resolve, reject);
                }, interval);
            } catch (e) {
              setTimeout(function () {
                transactionReceiptAsync(txnHash, resolve, reject);
              }, interval);
            }
          }
        } else resolve(receipt);
      }
    } catch (e) {
      reject(e);
    }
  };

  if (Array.isArray(txnHash)) {
    var promises = [];
    txnHash.forEach(function (oneTxHash) {
      promises.push(awaitTx(web3, oneTxHash, options));
    });
    return Promise.all(promises);
  } else {
    return new Promise(function (resolve, reject) {
      transactionReceiptAsync(txnHash, resolve, reject);
    });
  }
}
