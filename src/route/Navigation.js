import {NavigationContainer} from '@react-navigation/native';
import * as React from 'react';

import MainNavigator from './MainNavigator';

export default function Navigation() {
  return (
    <NavigationContainer>
      <MainNavigator />
    </NavigationContainer>
  );
}
