/* eslint-disable react-native/no-inline-styles */
/* eslint-disable sort-keys */
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import React from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import CreateScreen from '../screens/Create/stack';
import ExploreScreen from '../screens/explore/stack';
import HomeScreen from '../screens/home/stack';
import MoreScreen from '../screens/more/stack';
import ProfileScreen from '../screens/profile/stack';
const BottomTabNavigator = ({ navigation, route }) => {
  const Tab = createBottomTabNavigator();
  return (
    <Tab.Navigator
      initialRouteName="HomeScreen"
      tabBarOptions={{
        inactiveTintColor: '#DADADA',
        showLabel: true,
        keyboardHidesTabBar: Platform.OS !== 'ios',
        style: {
          backgroundColor: '#fff',
          // paddingBottom:20,
          minHeight: 80,
        },
      }}>
      <Tab.Screen
        name="Home"
        options={{
          tabBarLabel: '',
          headerShown: false,
          // headerStyle: {
          //   height: Platform.OS=='ios'?20:0, // Specify the height of your custom header
          // },
          tabBarIcon: ({ color }) => (
            <View style={{ paddingTop: 10 }}>
              <MaterialIcons
                style={{ alignSelf: 'center' }}
                name="home"
                size={26}
                color={color}
              />
              <Text style={{ fontSize: 12, color: color }}>Home</Text>
            </View>
          ),
        }}
        component={HomeScreen}
      />
      <Tab.Screen
        name="ExploreScreen"
        options={{
          tabBarLabel: '',
          headerShown: false,
          headerStyle: {
            height: Platform.OS == 'ios' ? 20 : 0, // Specify the height of your custom header
          },
          tabBarIcon: ({ color }) => (
            <View style={{ paddingTop: 10 }}>
              <Fontisto
                style={{ alignSelf: 'center' }}
                name="earth"
                size={26}
                color={color}
              />
              <Text style={{ fontSize: 12, color: color }}>Explore</Text>
            </View>
          ),
        }}
        component={ExploreScreen}
      />
      <Tab.Screen
        name="Search"
        options={{
          tabBarLabel: '',
          headerShown: false,
          headerStyle: {
            height: Platform.OS == 'ios' ? 20 : 0, // Specify the height of your custom header
          },
          tabBarIcon: ({ color }) => (
            <View style={{ paddingTop: 10 }}>
              <Ionicons
                style={{ alignSelf: 'center' }}
                name="add-circle"
                size={26}
                color={color}
              />
              <Text style={{ fontSize: 12, color: color }}>Create</Text>
            </View>
          ),
        }}
        component={CreateScreen}
      />

      <Tab.Screen
        name="Profile"
        options={{
          tabBarLabel: '',
          headerShown: false,
          headerStyle: {
            height: Platform.OS == 'ios' ? 20 : 0, // Specify the height of your custom header
          },
          tabBarIcon: ({ color }) => (
            <View style={{ paddingTop: 10 }}>
              <Ionicons
                style={{ alignSelf: 'center' }}
                name="ios-person-outline"
                size={26}
                color={color}
              />
              <Text style={{ fontSize: 12, color: color }}>Profile</Text>
            </View>
          ),
        }}
        component={ProfileScreen}
      />
      {/* <Tab.Screen
        name="More"
        options={{
          tabBarLabel: '',
          headerShown: false,
          headerStyle: {
            height: Platform.OS == 'ios' ? 20 : 0, // Specify the height of your custom header
          },
          tabBarIcon: ({ color }) => (
            <View style={{ paddingTop: 10 }}>
              <Ionicons
                style={{ alignSelf: 'center' }}
                name="ios-menu"
                size={26}
                color={color}
              />
              <Text style={{ fontSize: 12, color: color }}>More</Text>
            </View>
          ),
        }}
        component={MoreScreen}
      /> */}
    </Tab.Navigator>
  );
};
export default BottomTabNavigator;
const styles = StyleSheet.create({});
