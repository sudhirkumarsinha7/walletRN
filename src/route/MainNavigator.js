import {createNativeStackNavigator} from '@react-navigation/native-stack';
import * as React from 'react';

import BottomTabNavigator from './BottomTabNavigator';

export default function MainNavigator() {
  const Stack = createNativeStackNavigator();

  const headerOptions = {
    headerStyle: {
      backgroundColor: '#1a51a2',
    },
    headerTintColor: '#000',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
    // eslint-disable-next-line sort-keys
    headerLayoutPresent: 'center',
  };
  return (
    <Stack.Navigator screenOptions={headerOptions} headerMode="float">
      <Stack.Screen
        options={{headerShown: false}}
        name="dashboard"
        component={BottomTabNavigator}
      />
    </Stack.Navigator>
  );
}
