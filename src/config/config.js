

/* eslint-disable sort-keys */
// export const DEV_URL = 'https://nft.statledger.io';
import { Buffer } from 'buffer';

// export const DEV_URL = 'https://tollytokens.com';
export const DEV_URL = 'https://test.tollytokens.com';

export const getIosDeviceTokeApi =
  'https://iid.googleapis.com/iid/v1:batchImport';
export const projectid = 'com.thegmrcargo';

/*Infura setup start*/
export const projectId = '2DTf2K1f9CHbXDp04vtmg06MLKz';   // <---------- your Infura Project ID
export const projectSecret = '711d290b49ffe6e0f1196f980a52489a';  // <---------- your Infura Secret
// (for security concerns, consider saving these values in .env files)
export const auth = 'Basic ' + Buffer.from(projectId + ':' + projectSecret).toString('base64');
/*Infura setup end*/
//https://tollytokens.com/api/catalogue_service/items/items?state=PURCHASED&userId=USR-10036&user_id=62bee1c13837e30d6
//https://tollytokens.com/api/catalogue_service/items/items?state=ON_SALE&userId=USR-10036&user_id=62bee1c13837e30d62111465
//https://tollytokens.com/api/catalogue_service/items/items?state=UNLISTED&userId=USR-10036&user_id=62bee1c13837e30d62111465
//https://tollytokens.com/api/catalogue_service/items/items?state=ON_SALE&userId=USR-10036&user_id=62bee1c13837e30d62111465
export const global = {
  supportMail: 'dev@statwig.com',
  supportPh: '+919059687874',
  GoogleMapsApiKey: 'AIzaSyBLwFrIrQx_0UUAIaUwt6wfItNMIIvXJ78',
  region: {
    latitude: 17.4295865,
    longitude: 78.368776,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  }
};
export function config() {
  const confs = {
    dev: {
      //nft normal API
      popularCollections: `${DEV_URL}/api/analytics_service/explore/popularCollections`,
      topSellers: `${DEV_URL}/api/analytics_service/explore/topSellers`,
      categories: `${DEV_URL}/api/catalogue_service/categories/`,
      filterCollections: `${DEV_URL}/api/catalogue_service/collections/filterCollections?`,
      filtermarket: `${DEV_URL} /api/catalogue_service/collections/filterCollections?`,
      getItems: `${DEV_URL}/api/catalogue_service/items/items?`,
      getNftDetails: `${DEV_URL}/api/catalogue_service/items/item/`,

      gettredingList: `${DEV_URL}/api/catalogue_service/items/items?state=ON_AUCTION`,
      getColectionList: `${DEV_URL}/api/catalogue_service/items/items?collectionId=`,
      uploadImage: `https://ipfs.infura.io:5001/api/v0/add?stream-channels=true&progress=false`,

      ethtousdconvert: `https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=USD`,

      login: `${DEV_URL}/api/user_service/login`,
      register: `${DEV_URL}/api/user_service/register`,
      deleteAccount: `${DEV_URL}/api/user_service/deactivate`,


      //User details
      userCreatedDetails: `${DEV_URL}/api/catalogue_service/items/items?state=ON_SALE&userId=630dbe97ed8902ab0e532be1`,
      userPerchageDetails: `${DEV_URL}/api/catalogue_service/items/items?state=PURCHASED&userId=630dbe97ed8902ab0e532be1`,
      userUnlistedDetails: `${DEV_URL}/api/catalogue_service/items/items?state=UNLISTED&userId=630dbe97ed8902ab0e532be1`,
      userFavDetails: `${DEV_URL}/api/analytics_service/analytics/getFavItemByOwner/630dbe97ed8902ab0e532be1`,
      userCollectionDetails: `${DEV_URL}/api/catalogue_service/collections/getCollectionByOwner/630dbe97ed8902ab0e532be1`,

    }
      
  };

  const conf = confs.dev;

  return conf;
}
