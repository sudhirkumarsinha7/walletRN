/* eslint-disable no-sparse-arrays */
import { Dimensions } from 'react-native';
export const DeviceHeight = Dimensions.get('window').height;
export const DeviceWidth = Dimensions.get('window').width;
export const imageslist = [
  { image: require('../assets/Img/art.png') },
  { image: require('../assets/Img/digital.png') },
  { image: require('../assets/Img/other.png') },
  { image: require('../assets/Img/1.png') },
  { image: require('../assets/Img/2.png') },
  { image: require('../assets/Img/3.png') },
  { image: require('../assets/Img/4.png') },
  { image: require('../assets/Img/5.png') },
  ,
];
export const localImage = {
  art: { image: require('../assets/Img/art.png') },
  digital:{ image: require('../assets/Img/digital.png') },
  digital: { image: require('../assets/Img/digital.png') },
  loading:{ image: require('../assets/loading.gif') },
};
export const userInfo = {
  name: 'Xyz ',
  email: 'abc@gmail.com',
  comment: 'Why Netflix & Chill when you can NFT & Chill? 😎',
};
export const userNftList = [
  { name: 'User Created', id: 1 },
  { name: 'User Purchase', id: 2 },
  { name: 'User Unlisted', id: 3 },
  { name: 'My Collection', id: 4 },
  { name: 'Favarotes', id: 5 },
];
export const Colors = {
  red00: 'red ',
  blue: 'blue',
  black: 'black',
};
