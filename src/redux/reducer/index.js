import {combineReducers} from 'redux';
import AsyncStorage from "@react-native-async-storage/async-storage";
import {
  persistReducer,
} from 'redux-persist'
import auth from './auth';
import home from './home';
import user from './user'
const persistConfigAuth = {
  key: 'auth',
  storage: AsyncStorage,
  whitelist: ['userinfo'],
}
const persistConfigUser = {
  key: 'user',
  storage: AsyncStorage,
  whitelist: ['walletAddress'],
}


export default combineReducers({
  home,
  auth:persistReducer(persistConfigAuth, auth),
  user:persistReducer(persistConfigUser, user),

});
