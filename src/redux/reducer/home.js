/* eslint-disable sort-keys */
import {
  CATEGOERY,
  ETH_TO_USD,
  FILTER_COLLECTION,
  FILTER_COLLECTION_WITH_CATEGORY,
  GET_ITEMS,
  GET_TREDING_DEALS,
  POPULER_COLLECTION,
  SELECTED_NFT,
  TOPSHELLER
} from '../constant';

const initialState = {
  top_seller_list: [],
  popular_collection_list: [],
  category_list: [],
  flter_collection: [],
  item_list: [],
  treding_Deals: [],
  explore_particular_collection: [],
  eth_usd: {},
  selected_nft: {},
};
export default function (state = initialState, action) {
  switch (action.type) {
    case TOPSHELLER:
      return {
        ...state,
        top_seller_list: action.payload,
      };
    case SELECTED_NFT:
      return {
        ...state,
        selected_nft: action.payload,
      };
    case ETH_TO_USD:
      return {
        ...state,
        eth_usd: action.payload,
      };
    case POPULER_COLLECTION:
      return {
        ...state,
        popular_collection_list: action.payload,
      };
    case FILTER_COLLECTION_WITH_CATEGORY:
      return {
        ...state,
        explore_particular_collection: action.payload,
      };
    case GET_TREDING_DEALS:
      return {
        ...state,
        treding_Deals: action.payload,
      };
    case CATEGOERY:
      return {
        ...state,
        category_list: action.payload,
      };
    case FILTER_COLLECTION:
      return {
        ...state,
        flter_collection: action.payload,
      };
    case GET_ITEMS:
      return {
        ...state,
        item_list: action.payload,
      };
    default:
      return state;
  }
}
