/* eslint-disable functional/no-let */
/* eslint-disable sort-keys */
import axios from 'axios';

import {config, getIosDeviceTokeApi,auth} from '../../config/config';
import {
  CATEGOERY,
  ETH_TO_USD,
  FILTER_COLLECTION,
  FILTER_COLLECTION_WITH_CATEGORY,
  GET_ITEMS,
  GET_TREDING_DEALS,
  POPULER_COLLECTION,
  SELECTED_NFT,
  TOPSHELLER,
} from '../constant';

import {loadingOff, loadingOn} from './loader';

// import AsyncStorage from '@react-native-async-storage/async-storage';
export const uploadImage =
  (data) =>
  async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      let response;
      const configs = {
        headers: {
          'content-type': 'multipart/form-data',
          'authorization':'Basic MkRUZjJLMWY5Q0hiWERwMDR2dG1nMDZNTEt6OjcxMWQyOTBiNDlmZmU2ZTBmMTE5NmY5ODBhNTI0ODlh'
        },
      };
      console.log(
        'Action_uuploadImagesUrl ' + JSON.stringify(`${config().uploadImage}`),
      );
      console.log('Action_upload_Image ', data);
      response = await axios.post(config().uploadImage, data, configs);
      console.log('Action_upload_Image_response' + JSON.stringify(response));
      return response;
    } catch (e) {
      console.log('Action_upload_Image_error' + JSON.stringify(e));
    } finally {
      loadingOff(dispatch);
    }
  };
export const ethToUSDConvertor = () => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
  
      const response = await axios.get(config().ethtousdconvert);
      console.log('ActionethToUSDConvertor_response' + JSON.stringify(response));
      dispatch({
        type: ETH_TO_USD,
        payload: response.data,
      });
  
      return response;
    } catch (e) {
      console.log('Action_ethToUSDConvertor_error' + JSON.stringify(e));
    } finally {
      loadingOff(dispatch);
    }
  };
export const getTopSheller = () => async (dispatch, getState) => {
  loadingOn(dispatch);
  try {
    console.log('Action_getTopSheller' + config().topSellers);

    const response = await axios.get(config().topSellers);
    console.log('Action_getTopSheller_response' + JSON.stringify(response));
    dispatch({
      type: TOPSHELLER,
      payload: response.data.data,
    });

    return response;
  } catch (e) {
    console.log('Action_getTopShellero_error' + JSON.stringify(e));
  } finally {
    loadingOff(dispatch);
  }
};
export const getPopularCollection = () => async (dispatch, getState) => {
  loadingOn(dispatch);
  try {
    console.log('Action_getPopularCollection' + config().popularCollections);

    const response = await axios.get(config().popularCollections);
    console.log(
      'Action_getPopularCollection_response' + JSON.stringify(response),
    );
    dispatch({
      type: POPULER_COLLECTION,
      payload: response.data.data,
    });

    return response;
  } catch (e) {
    console.log('Action_ggetPopularCollection_error' + JSON.stringify(e));
  } finally {
    loadingOff(dispatch);
  }
};

export const getCategoryList = () => async (dispatch, getState) => {
  loadingOn(dispatch);
  try {
    console.log('Action_getCategoryList' + config().categories);

    const response = await axios.get(config().categories);
    console.log('Action_getCategoryList_response' + JSON.stringify(response));
    let cat =response.data && response.data.data;
    let all= {
      "categoryName":"All",
      "id":'All'
     }
    cat.unshift(all)
    dispatch({
      type: CATEGOERY,
      payload: cat,
    });

    return response;
  } catch (e) {
    dispatch({
      type: CATEGOERY,
      payload: [ {
        "categoryName":"All",
        "id":'All'
       }],
    });
    console.log('Action_getCategoryList_error' + JSON.stringify(e));
  } finally {
    loadingOff(dispatch);
  }
};
export const getfilterCollection = (id='All') => async (dispatch, getState) => {
  loadingOn(dispatch);
  try {
    dispatch({
      type: FILTER_COLLECTION,
      payload: [],
    });
    console.log('Action_getfilterCollection' + config().filterCollections);
    let url = '';
    if(id=='All'){
      url = config().filterCollections
    }else{
      url = config().filterCollections+'categoryId='+id
    }
    const response = await axios.get(url);
    console.log(
      'Action_getfilterCollection_response' + JSON.stringify(response),
    );
    dispatch({
      type: FILTER_COLLECTION,
      payload: response.data.data,
    });

    return response;
  } catch (e) {
    console.log('Action_getfilterCollection_error' + JSON.stringify(e));
  } finally {
    loadingOff(dispatch);
  }
};
export const getList = () => async (dispatch, getState) => {
  loadingOn(dispatch);
  try {
    console.log('Action_ggetListr' + config().getItems);

    const response = await axios.get(config().getItems);
    console.log('Action_getList_response' + JSON.stringify(response));
    dispatch({
      type: GET_ITEMS,
      payload: response.data.data,
    });

    return response;
  } catch (e) {
    console.log('Action_getList_error' + JSON.stringify(e));
  } finally {
    loadingOff(dispatch);
  }
};
export const getTredingList = () => async (dispatch, getState) => {
  loadingOn(dispatch);
  try {
    console.log('Action_getTredingList' + config().gettredingList);

    const response = await axios.get(config().gettredingList);
    console.log('Action_getTredingList_response' + JSON.stringify(response));
    dispatch({
      type: GET_TREDING_DEALS,
      payload: response.data.data && response.data.data.items,
    });

    return response;
  } catch (e) {
    console.log('Action_getList_error' + JSON.stringify(e));
  } finally {
    loadingOff(dispatch);
  }
};
export const getfilterCollectionParticularList = (id) => async (dispatch, getState) => {
  loadingOn(dispatch);
  try {
    dispatch({
      type: FILTER_COLLECTION_WITH_CATEGORY,
      payload: [],
    });
    console.log('Action_getfilterCollectionParticularList' + config().getColectionList+id);
    const response = await axios.get(config().getColectionList+id);
    console.log(
      'Action_getfilterCollectionParticularList_response' + JSON.stringify(response),
    );
    dispatch({
      type: FILTER_COLLECTION_WITH_CATEGORY,
      payload:  response.data.data && response.data.data.items,
    });

    return response;
  } catch (e) {
    console.log('Action_getfilterCollectionParticularList_error' + JSON.stringify(e));
  } finally {
    loadingOff(dispatch);
  }
};
export const getNftDetails = (id) => async (dispatch, getState) => {
  loadingOn(dispatch);
  try {
    console.log('Action_ggetNftDetails' + config().getNftDetails+id);
    const response = await axios.get(config().getNftDetails+id);
    console.log(
      'Action_getNftDetails_response' + JSON.stringify(response),
    );
    dispatch({
      type: SELECTED_NFT,
      payload:  response.data.data,
    });

    return response;
  } catch (e) {
    console.log('Action_getNftDetails_error' + JSON.stringify(e));
  } finally {
    loadingOff(dispatch);
  }
};
