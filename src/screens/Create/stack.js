import {createNativeStackNavigator} from '@react-navigation/native-stack';
import * as React from 'react';


import Create from './index';
const Stack = createNativeStackNavigator();
const CreateScreen = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Create"
        component={Create}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
};
export default CreateScreen;
