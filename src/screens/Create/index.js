/* eslint-disable no-undef */
/* eslint-disable react-native/no-color-literals */
/* eslint-disable functional/no-let */
/* eslint-disable sort-keys */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable react-native/no-inline-styles */
import { Buffer } from 'buffer';

import axios from 'axios';
import { useFormik } from 'formik';
// import IPFS from 'ipfs-mini';
import React, { useEffect, useState } from 'react';
import {
  Image,
  Keyboard,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { Switch } from 'react-native-paper';


import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { connect } from 'react-redux';
import * as Yup from 'yup';
import { mintToken } from '../../NftBlockchain/market';
import {
  DeviceHeight,
  DeviceWidth,
  imageslist,
} from '../../config/global';
import { uploadImage } from '../../redux/action/home';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Foundation from 'react-native-vector-icons/Foundation';
import { PropertisInfo, SliderScreen, StatusScreen } from '../../components/Explore/helper'
import { Colors } from "../../components/Common/Style";
import { moderateScale, scale, verticalScale } from "../../components/Scale";
import { CreateNftData } from "../../components/Common/default";
import { InputTextHelper } from "../../components/Common/heper";
import { PropertiesPopUP, PopUp } from '../../components/PopUp';
import WalletConnectProvider from '@walletconnect/web3-provider';
import { useWalletConnect, withWalletConnect } from '@walletconnect/react-native-dapp';
import {projectId,projectSecret} from '../../config/config'
const contractaddress = "0x05d2259Eb45309De47C267638721Cd91e7470680";
var ethers = require('ethers');

// const IPFS = require('ipfs-mini');


export const nftaddress = '0x41D3F0070c92375BF21e526c31959a42306d1455';
// const IPFS = require('ipfs-mini');
const CreateNft = props => {
  const [isMandatory, setISmadatory] = useState('');
  const [imagePath, setImagePAth] = useState('');
  const [imageUrl, setimageUrl] = useState('');
  const connector = useWalletConnect();
  const [isModalVisible, setIsModalVisible] = useState(false)
  const [isLevelModal, setLevelModal] = useState(false)
  const [isSkillModal, setSkillModal] = useState(false)
  const [isModalVisible2, setIsModalVisible2] = useState(false)
  const [errorMessage, setErrorMessage] = useState('Issue in API side');
  const [successMessage, setSuccessMessage] = useState('Your NFT Created successfully');
  const [properrtisList, setProperrtisList] = React.useState([{ 'name': '', 'value': '' }]);
  const [skillList, setSkillList] = React.useState([{ 'name': '', 'value': '' }]);
  const [levelList, setLevelList] = React.useState([{ 'name': '', 'value': '' }]);
  const [InAuction, setAuction] = useState(false)
  const [isEnabledBundle, setIsEnabledBundle] = useState(false);
  const toggleSwitch = () => setIsEnabledBundle(previousState => !previousState);
  const [specificBuyer, setSpecificBuyer] = useState(false);
  const specificBuyerSwitch = () => setSpecificBuyer(previousState => !previousState);

  const onConnect = async () => {
    connector.connect()

    const provider = new WalletConnectProvider({
      bridge: "https://bridge.walletconnect.org",
      rpc: { 1: "https://rpc.ankr.com/eth", 3: "https://ropsten.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161" },
      infuraId: "8043bb2cf99347b1bfadfb233c5325c0",
      connector: connector,
      connected: true,
      qrcode: false,
    });
    await provider.enable();
    console.log("connector", connector)
    const ethers_provider = new ethers.providers.Web3Provider(provider);
    // const signer = await ethers_provider.getSigner();
    // console.log("signer", signer)
    // const userNetwork = await ethers_provider.getNetwork();
    // console.log("userNetwork", userNetwork)

    // console.log("addresss", await signer.getAddress())

    // console.log("accounts22", signer.provider.provider.accounts[0])
    // const balance = await ethers_provider.getBalance(signer.provider.provider.accounts[0]);
    // console.log("balance", balance)
    // const instance = new ethers.Contract(contractaddress, abi, ethers_provider);
    // console.log("INtsance is", instance);
    // const c = ethers.utils.parseUnits("1", 'ether')
   //  const b = await sendTx(instance['connect'](signer), "load", [c])
   //  console.log("B", b)
    // const account = await signer.getAccounts();


    return ethers_provider;

  }
  var validationSchema = Yup.object().shape(
    {
      // image: Yup.string().required('image Required'),
    },
    [''],
  ); // <-- HERE!!!!!!!!
  const createForm = useFormik({
    initialValues: {
      image: 'ddd',
      _assetname: '',
      _externallink: '',
      _assetdesc: '',
      _properties: '',
      _stats: '',
      _levels: '',
      _sensitivecontent: false,
      _unlockablecontent: false,
      _supply: 1,
      _blockchainType: 'Rinkeby',
      mintedBy: '',
    },
    validationSchema,
    validateOnChange: false,
    onSubmit: (values, actions) => {
      handleSubmitOrder({ ...values });
    },
  });

  const handleSubmitOrder = async values => {
    console.log('valuuu', values);
    // API Call
    let data = JSON.stringify({
      image: imageUrl||'https://ipfs.infura.io/ipfs/QmWCEDYxixGVQvxTBVrtBmmeS3zJCAqxaZDaSU1JWZWLBh',
      _assetname: 'Test RN',
      _externallink: 'externalLink',
      _assetdesc: 'description',
      _sensitivecontent: false,
      _unlockablecontent: false,
      _supply: 1,
      _blockchainType: 'Rinkeby',
    });
    const newdata = new FormData();
    newdata.append({
      name: 'filename',
      filename: '',
      data
    });
    console.log('data' + JSON.stringify(newdata));
    console.log("connector", connector)

    // let result = await props.uploadImage(newdata);
    // let url = result.data && result.data.Hash;

    // console.log('image url' + url);
    // var dataURL = `https://ipfs.infura.io/ipfs/${url}`;
    // console.log('image dataURL' + dataURL);
    var dataURL ='https://ipfs.infura.io/ipfs/QmbyswsHbp3UtziX8FsAdxS1Mgmi75FeT8D7Et9vhkinSM'
    console.log('dataURL' + dataURL);

    const provid = await onConnect()
    console.log('provid',provid);

    const tokenId = await mintToken(dataURL, provid);
    console.log('tokenId' + tokenId);
    // setIsModalVisible2(true)

  };

  const _CreateOrder = () => {
    Keyboard.dismiss();
    createForm.handleSubmit();
  };
  const chooseFile = () => {
    var options = {
      title: 'Select Image',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    launchImageLibrary(options, response => {
      console.log('User response' + JSON.stringify(response));
      if (response.didCancel) {
        console.log('User canceled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        // alert(response.customButton);
      } else if (response.assets) {
        setImagePAth(response.assets[0]);
        // this.setState({ filePath: response });
        handleUploadPhoto(response.assets[0]);
      }
    });
  };
  const handleUploadPhoto = async response => {
    const data = new FormData();
    let filePath = response;
    // const auth = 'Basic ' + Buffer.from(projectId + ':' + projectSecret).toString('base64');
    // const auth = 'Basic ' +(projectId + ':' + projectSecret);
    const auth = 'Basic MkRUZjJLMWY5Q0hiWERwMDR2dG1nMDZNTEt6OjcxMWQyOTBiNDlmZmU2ZTBmMTE5NmY5ODBhNTI0ODlh' ;


    console.log('User filePath ' + JSON.stringify(filePath));
    console.log('User projectId' ,projectId);
    console.log('User projectSecret' ,projectSecret);
    console.log('User auth' ,auth);

    data.append('photo', {
      name: filePath.fileName,
      type: filePath.type,
      uri:
        Platform.OS === 'android'
          ? filePath.uri
          : filePath.uri.replace('file://', ''),
    });
    let result = await props.uploadImage(data);
    // console.log('image result' + JSON.stringify(result));

    // const img = Platform.OS === 'android'? filePath.uri: filePath.uri.replace('file://', '');
    // const ipfs = create({
    //   host: "ipfs.infura.io",
    //   port: 5001,
    //   protocol: "https",
    //   headers: {
    //     authorization: auth,
    // },
    // });
    // const ipfs = new IPFS({
    //   host: 'ipfs.infura.io',
    //   port: 5001,
    //   protocol: 'https',
    //   headers: {
    //     authorization: auth,
    // }
    // });
    // // console.log('User ipfs' ,ipfs);

    // let buf = await Buffer(img)
    // const added = await ipfs.add(buf);
    // const imgURl = `https://ipfs.infura.io/ipfs/${added}`;
    let url = result.data && result.data.Hash;

    console.log('image url' + url);
    // var imgURl = `https://ipfs.io/ipfs/${url}`;
    var imgURl = `https://ipfs.infura.io/ipfs/${url}`;

    console.log('image imgURl' + imgURl);
    setimageUrl(imgURl);
  };
  const ok = () => {
    setIsModalVisible(false)
    setIsModalVisible2(false)
    setSkillModal(false)
    setLevelModal(false)
  }
  const cancel = () => {
    setIsModalVisible(false)
    setIsModalVisible2(false)
    setSkillModal(false)
    setLevelModal(false)
  }
  const _add_another_Property = () => {
    let newList = [
      ...properrtisList,
      { 'name': '', 'value': '' }
    ]
    setProperrtisList(newList);
  }
  const cancelProperty = index => {
    const propertiesListClone = JSON.parse(JSON.stringify(properrtisList));
    propertiesListClone.splice(index, 1);
    setProperrtisList(propertiesListClone);
  };
  const _add_another_Skills = () => {
    let newList = [
      ...skillList,
      { 'name': '', 'value': '' }
    ]
    setSkillList(newList);
  }
  const cancelSkill = index => {
    const SkillListClone = JSON.parse(JSON.stringify(skillList));
    SkillListClone.splice(index, 1);
    setSkillList(SkillListClone);
  };
  const _add_another_Level = () => {
    let newList = [
      ...levelList,
      { 'name': '', 'value': '' }
    ]
    setLevelList(newList);
  }
  const cancelLevel = index => {
    const levelListtClone = JSON.parse(JSON.stringify(levelList));
    levelListtClone.splice(index, 1);
    setLevelList(levelListtClone);
  };
  return (
    <ScrollView style={{ marginLeft: scale(15), marginRight: scale(15) }}>
      <PropertiesPopUP
        isModalVisible={isModalVisible}
        label="Save"
        Ok={() => ok()}
        cancel={() => cancel()}
        data={properrtisList}
        setData={setProperrtisList}
        add_another={_add_another_Property}
        delete={cancelProperty}
        firstLine={'Type'}
        secondLine={'Name'}
        title={'Add Properties'}
        decription={`Properties show up underneath your item, are clickable, and can be filtered in your collection's sidebar.`}
      />
      <PropertiesPopUP
        isModalVisible={isLevelModal}
        label="Save"
        Ok={() => ok()}
        cancel={() => cancel()}
        data={levelList}
        setData={setLevelList}
        add_another={_add_another_Level}
        delete={cancelLevel}
        firstLine={'Name'}
        secondLine={'Value'}
        title={'Add Level'}
        decription={`Levels show up underneath your item, are clickable, and can be filtered in your collection's sidebar.`}
      />
      <PropertiesPopUP
        isModalVisible={isSkillModal}
        label="Save"
        Ok={() => ok()}
        cancel={() => cancel()}
        data={skillList}
        setData={setSkillList}
        add_another={_add_another_Skills}
        delete={cancelSkill}
        firstLine={'Name'}
        secondLine={'Value'}
        title={'Add Skills'}
        decription={`Stats show up underneath your item, are clickable, and can be filtered in your collection's sidebar.`}
      />
      <PopUp
        isModalVisible={isModalVisible2}
        image={require('../../assets/success.png')}
        text="Error"
        text1={successMessage}
        label1="Cancel"
        label="Done"
        Ok={() => cancel()}
        cancel={() => cancel()}
      />
      <View style={{ marginTop: 50 }}>
        <Text style={{ fontSize: scale(24), }}>{'Create your NFT'}</Text>
        <View
          style={{
            borderRadius: 10,
            padding: 5,
          }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <Text
              style={{
                marginLeft: 10,
                fontWeight: 'bold',
              }}>
              {'Upload Image'}
            </Text>
            <View
              style={{
                flex: 3,
                marginRight: 1,
                justifyContent: 'flex-end',
                alignItems: 'flex-end',
              }}>
              <TouchableOpacity
                style={{
                  width: 105,
                  height: 35,
                  borderRadius: 10,
                  backgroundColor: Colors.blueFF,
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderColor: '#58b1da',
                  borderWidth: 1,
                  flexDirection: 'row',
                }}
                onPress={() => chooseFile()}>
                <Text
                  style={{
                    fontSize: 16,
                    color: '#FFFFFF',
                    fontWeight: 'bold',
                  }}>
                  {'   ' + 'Upload'}
                </Text>
              </TouchableOpacity>
            </View>
          </View>

          <View
            style={{
              borderStyle: 'dashed',
              borderRadius: 10,
              borderWidth: 1,
              borderColor: 'gray',
              padding: 5,
              backgroundColor: '#FFFFFF',
              marginTop: 10,
            }}>
            {imagePath && imagePath.uri ? (
              <Image
                style={{ height: 240, borderStyle: 'dotted',width:340 }}
                resizeMode="cover"
                source={{
                  uri: imagePath.uri,
                }}
              />
            ) : (
              <TouchableOpacity
                onPress={() => chooseFile()}
                style={{
                  marginTop: 10,
                  alignItems: 'center',
                  justifyContent: 'center',
                  width: 328,
                  height: 240,
                }}>
                <View
                  style={{
                    borderRadius: 10,
                    backgroundColor: Colors.blueFF,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderColor: '#58b1da',
                    borderWidth: 1,
                    padding: 10,
                  }}>
                  <Text
                    style={{
                      fontSize: 16,
                      color: '#FFFFFF',
                      fontWeight: 'bold',
                    }}>
                    {'Browse Files'}
                  </Text>
                </View>
              </TouchableOpacity>
            )}
          </View>
        </View>

        <InputTextHelper
          eachRow={CreateNftData._assetname}
          state={createForm.values}
          Form={createForm}
          updateState={createForm.handleChange('_assetname')}
        />
        <InputTextHelper
          eachRow={CreateNftData._externallink}
          state={createForm.values}
          Form={createForm}
          updateState={createForm.handleChange('_externallink')}
        />
        <InputTextHelper
          eachRow={CreateNftData._assetdesc}
          state={createForm.values}
          Form={createForm}
          updateState={createForm.handleChange('_assetdesc')}
        />
        <View style={{ flexDirection: 'row', borderRadius: scale(5), marginTop: scale(10), }}>
          <TouchableOpacity onPress={() => setIsModalVisible(true)} style={{ borderWidth: 1, borderColor: Colors.blueFc, paddingLeft: scale(10), paddingRight: scale(10), borderRadius: scale(5), padding: scale(5) }}>

            <Text style={{ textAlign: 'center', fontSize: scale(14), color: Colors.blueFc }}>{'+ Property'}</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => setLevelModal(true)} style={{ borderWidth: 1, borderColor: Colors.blueFc, paddingLeft: scale(10), paddingRight: scale(10), borderRadius: scale(5), padding: scale(5), marginLeft: scale(15), marginRight: scale(15) }}>

            <Text style={{ textAlign: 'center', fontSize: scale(14), color: Colors.blueFc }}>{'+ Level'}</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => setSkillModal(true)} style={{ borderWidth: 1, borderColor: Colors.blueFc, paddingLeft: scale(10), paddingRight: scale(10), borderRadius: scale(5), padding: scale(5) }}>

            <Text style={{ textAlign: 'center', fontSize: scale(14), color: Colors.blueFc }}>{'+ Skills'}</Text>
          </TouchableOpacity>

        </View>
        <PropertisInfo
          properties={properrtisList || []}
        />
        <SliderScreen
          levels={levelList || []}
        />
        <StatusScreen
          stats={skillList || []}
        />

        <Text style={{ fontSize: scale(24), marginTop: scale(10) }}>{'List item for sale'}</Text>
        <Text style={{ fontSize: scale(14), marginTop: scale(10) }}>{'Type'}</Text>

        <View style={{ flexDirection: 'row', borderRadius: scale(5), marginTop: scale(10), alignItems: 'space-between' }}>
          <TouchableOpacity onPress={() => setAuction(false)} style={{ borderWidth: 1, flex: 0.5, borderColor: Colors.blueFc, backgroundColor: InAuction ? Colors.whiteFF : Colors.blueFc, paddingLeft: scale(10), paddingRight: scale(10), borderRadius: scale(5), padding: scale(5), justifyContent: 'center', alignItems: 'center' }}>
            <FontAwesome name="user" size={25} color={InAuction ? Colors.blueFc : Colors.whiteFF} />

            <Text style={{ textAlign: 'center', fontSize: scale(16), color: InAuction ? Colors.blueFc : Colors.whiteFF, }}>{'Fixed Price'}</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => setAuction(true)} style={{ borderWidth: 1, flex: 0.5, borderColor: Colors.blueFc, backgroundColor: InAuction ? Colors.blueFc : Colors.whiteFF, paddingLeft: scale(10), paddingRight: scale(10), borderRadius: scale(5), padding: scale(5), marginLeft: scale(15), justifyContent: 'center', alignItems: 'center' }}>
            <FontAwesome name="user" size={25} color={InAuction ? Colors.whiteF7 : Colors.blueFc} />
            <Text style={{ textAlign: 'center', fontSize: scale(16), color: InAuction ? Colors.whiteF7 : Colors.blueFc, }}>{'Timed Auction'}</Text>
          </TouchableOpacity>

        </View>
        <InputTextHelper
          eachRow={CreateNftData.price}
          state={createForm.values}
          Form={createForm}
          updateState={createForm.handleChange('price')}
        />
        <InputTextHelper
          eachRow={CreateNftData.royalty}
          state={createForm.values}
          Form={createForm}
          updateState={createForm.handleChange('royalty')}
        />
        <InputTextHelper
          eachRow={CreateNftData.duration}
          state={createForm.values}
          Form={createForm}
          updateState={createForm.handleChange('duration')}
        />
        <InputTextHelper
          eachRow={CreateNftData.category}
          state={createForm.values}
          Form={createForm}
          updateState={createForm.handleChange('category')}
        />
        <InputTextHelper
          eachRow={CreateNftData.collection}
          state={createForm.values}
          Form={createForm}
          updateState={createForm.handleChange('collection')}
        />
        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: scale(10) }}>
          <FontAwesome name="lock" size={25} color={Colors.black70} />

          <Text style={{ flex: 1, marginLeft: scale(10), fontSize: scale(14), color: Colors.black70 }}>Sell as a bundle</Text>
          <Switch
            trackColor={{ false: "#767577", true: "#81b0ff" }}
            thumbColor={isEnabledBundle ? "#f5dd4b" : "#f4f3f4"}
            style={{ transform: [{ scaleX: 0.5 }, { scaleY: 0.5 }] }}
            onValueChange={toggleSwitch}
            value={isEnabledBundle}
          />
        </View>
        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: scale(10) }}>
          <Foundation name="alert" size={25} color={Colors.black70} />

          <Text style={{ flex: 1, marginLeft: scale(10), fontSize: scale(14), color: Colors.black70 }}>Reserve for a specific buyer</Text>
          <Switch
            trackColor={{ false: "#767577", true: "#81b0ff" }}
            thumbColor={specificBuyer ? "#f5dd4b" : "#f4f3f4"}
            style={{ transform: [{ scaleX: 0.5 }, { scaleY: 0.5 }] }}
            onValueChange={specificBuyerSwitch}
            value={specificBuyer}
          />
        </View>
        <Text style={{ fontSize: scale(14), color: Colors.black0, marginTop: scale(10) }}>Fees</Text>

        <View style={{ flex: 1, flexDirection: 'row', marginTop: scale(10) }}>
          <Text style={{ flex: 0.9, fontSize: scale(12), color: Colors.black70 }}>Service Fee</Text>
          <Text style={{ fontSize: scale(12), color: Colors.black70, justifyContent: 'flex-end', flex: 0.1 }}>{'2.5%'}</Text>

        </View>
        <TouchableOpacity
          onPress={() => _CreateOrder()}
          style={{ backgroundColor: Colors.blueFc, padding: 15, borderRadius: scale(5), marginTop: scale(15), marginBottom: scale(15) }}>
          <Text style={{ textAlign: 'center', color: Colors.whiteFF, fontSize: scale(16), fontWeight: 'bold' }}>Sell Now</Text>
        </TouchableOpacity>
      </View>
    </ScrollView >
  );
};

function mapStateToProps(state) {
  return {
    top_seller_list: state.home.top_seller_list,
    popular_collection_list: state.home.popular_collection_list,
    category_list: state.home.category_list,
    flter_collection: state.home.flter_collection,
    item_list: state.home.item_list,
    userInfo: state.auth.userInfo,
  };
}

export default connect(mapStateToProps, { uploadImage })(CreateNft);
