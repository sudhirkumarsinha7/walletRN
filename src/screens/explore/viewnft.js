/* eslint-disable functional/immutable-data */
/* eslint-disable functional/no-let */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */

import React, { useState } from 'react';
import { ScrollView, Text, View } from 'react-native';
import { connect } from 'react-redux';

import { Colors } from '../../components/Common/Style';
import {
  NftBidHistory,
  NftInfo,
  NftImageView
} from '../../components/Explore/helper';
import { HeaderWithBack } from '../../components/Header';
import { moderateScale, scale, verticalScale } from '../../components/Scale';
import SelectionSelect from '../../components/SelectionSelect'

let radioItems = [
  {
    label: 'Info',
    selected: true,
  },
  {
    label: 'Bid History',
    selected: false,
  },
]
const ViewNFT = (props) => {
  const [selectedTab, setTab] = useState('Info');
  const [selectedTab1, setTab1] = useState('BID');

  const nftDetails = props.route.params.nftDetails;
  // console.log('porps.navigation ' + JSON.stringify(props.route.params.nftDetails))
  const changeActiveRadioButton = (index) => {
    radioItems.map(item => {
      item.selected = false;
    });

    radioItems[index].selected = true;
    setTab(radioItems[index].label)
  }
  return (
    <ScrollView>
      <HeaderWithBack
        navigation={props.navigation}
        name={'Explore'}
      />
      <NftImageView
        item={props.selected_nft}
        eth_usd={props.eth_usd} />
      <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: verticalScale(10), elevation: 5 }}>
        {radioItems.map((item, key) => (
          <SelectionSelect
            key={key}
            button={item}
            onClick={() => changeActiveRadioButton(key)}
            selectedTextColor={Colors.blueFc}
            unSelectedTextColor={Colors.whiteFF}
          />
        ))}
      </View>
      {selectedTab === 'Info' ? <NftInfo
        item={props.selected_nft}
        eth_usd={props.eth_usd} /> : <NftBidHistory
        item={props.selected_nft}
        eth_usd={props.eth_usd} />}
    </ScrollView>
  );
};
function mapStateToProps(state) {
  return {
    top_seller_list: state.home.top_seller_list,
    popular_collection_list: state.home.popular_collection_list,
    category_list: state.home.category_list,
    flter_collection: state.home.flter_collection,
    item_list: state.home.item_list,
    treding_Deals: state.home.treding_Deals,
    explore_particular_collection: state.home.explore_particular_collection,
    eth_usd: state.home.eth_usd,
    selected_nft: state.home.selected_nft
  };
}

export default connect(mapStateToProps, {})(ViewNFT);