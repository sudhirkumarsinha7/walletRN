/* eslint-disable eslint-comments/no-unused-disable */
/* eslint-disable eslint-comments/no-unused-disable */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import * as React from 'react';
import { FlatList, ScrollView, Text,View } from 'react-native';
import { connect } from 'react-redux';


import { Empty_Card } from '../../components/Card'
import {
  CategoryExploreList,
  CategoryListExpolre,
} from '../../components/Explore/helper';
import { HeaderWithBack } from '../../components/Header';
import {
  getNftDetails,
} from '../../redux/action/home';
const ViewExplore = (props) => {
  const eachExplore = each => {
    return (
      <View>
        <CategoryExploreList item={each}
          onChange={onChangeItemCategory}
          eth_usd={props.eth_usd} />
        {/* <Text>{JSON.stringify(each)}</Text> */}
      </View>
    );
  };
  const onChangeItemCategory = (item) => {
    console.log('onChangeItemCategory item'+JSON.stringify(item))
    props.getNftDetails(item.id)
    props.navigation.navigate('ViewNft',{nftDetails:item})
  }
  const showEmptyListView = () => {
    return <Empty_Card
      Text={'Data Empty'}
    />
  }
  return (
    <ScrollView>
       <HeaderWithBack
          navigation={props.navigation}
          name={'Explore'}
        />
      <FlatList
        onEndReachedThreshold={0.7}
        // horizontal={true}
        numColumns={2}
        keyExtractor={(item, index) => index.toString()}
        data={props.explore_particular_collection || []}
        ListEmptyComponent={showEmptyListView()}
        renderItem={({ item }) => eachExplore(item)}
      />
    </ScrollView>
  );
};
function mapStateToProps(state) {
  return {
    top_seller_list: state.home.top_seller_list,
    popular_collection_list: state.home.popular_collection_list,
    category_list: state.home.category_list,
    flter_collection: state.home.flter_collection,
    item_list: state.home.item_list,
    treding_Deals: state.home.treding_Deals,
    explore_particular_collection: state.home.explore_particular_collection,
    eth_usd: state.home.eth_usd,
  };
}

export default connect(mapStateToProps, {getNftDetails})(ViewExplore);

