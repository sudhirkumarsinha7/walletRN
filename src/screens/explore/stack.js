import {createNativeStackNavigator} from '@react-navigation/native-stack';
import * as React from 'react';

import ViewExplore from './ViewExplore';
import ViewNft from './viewnft';

import Explore from './index';
const Stack = createNativeStackNavigator();
const ExploreScreen = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Explore"
        component={Explore}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="ViewExplore"
        component={ViewExplore}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
};
export default ExploreScreen;
