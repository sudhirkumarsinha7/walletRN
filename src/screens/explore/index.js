
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable react-native/no-color-literals */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import { FlatList, Platform, StyleSheet, Text, View } from 'react-native';
import { connect } from 'react-redux';

import { Empty_Card } from '../../components/Card'
import {
  CategoryListExpolre,
  ExploreList,
} from '../../components/Explore/helper';
import {
  CategoryList,
  TopSellerComponent,
  TrendingList,
} from '../../components/Homehelper/helper';
import { scale } from '../../components/Scale';
import {
  getfilterCollection,
  getfilterCollectionParticularList,
  getList,
  getPopularCollection,
  getTopSheller,
  getTredingList,
  getNftDetails
} from '../../redux/action/home';

const ExploreScreen = props => {
  const [SelectedCategory, setCategory] = useState('All')
  const [CatList, setCategoryList] = useState([])

  useEffect(() => {
    //  let cat = props.category_list;
    //  let all= {
    //   "categoryName":"All",
    //   "id":'All'
    //  }
    //  let filterCat = cat.filter(each=>{
    //   if(each.categoryName==='All'){
    //     return each
    //   }
    //  })
    //  if(filterCat && filterCat.length){

    //  }else{
    //   cat.unshift(all)

    //  }
    //  setCategoryList(cat)
  }, []);
  const eachCat = each => {
    // console.log('eachCat ' + JSON.stringify(each));
    return (
      <View>
        <CategoryListExpolre item={each}
          SelectedCategory={SelectedCategory}
          onChangeCat={onChangeCategory}
        />

      </View>
    );
  };
  const onChangeCategory = (item) => {
    setCategory(item.categoryName)
    // console.log('getfilterCollection ' + JSON.stringify(item.id));
    props.getfilterCollection(item.id)
  }
  const onChangeItemCategory = (item) => {
    props.getfilterCollectionParticularList(item.id)
    props.navigation.navigate('ViewExplore')
  }
  const eachExplore = each => {
    return (
      <View>
        <ExploreList item={each}
          onChange={onChangeItemCategory} />
      </View>
    );
  };

  const eachTred = each => {
    return (
      <View>
        <TrendingList item={each}
          onChange={onChangeTrend} />
      </View>
    );
  };
  const onChangeTrend = (item) => {
    // console.log('onChangeTrend item' + JSON.stringify(item))
    props.getNftDetails(item.id)
    props.navigation.navigate('ViewNft', { nftDetails: item })
  }
  // console.log('props.popular_collection_list ' + JSON.stringify(props.popular_collection_list));
  const showEmptyListView = () => {
    return <Empty_Card
      Text={'Data Empty'}
    />
  }
  return (
    <View style={{ margin: 10, marginTop: Platform.OS === 'ios' ? scale(40) : 0 }}>
      <View style={{}}>
        <FlatList
          onEndReachedThreshold={0.7}
          horizontal={true}
          keyExtractor={(item, index) => index.toString()}
          data={props.category_list}
          renderItem={({ item }) => eachCat(item)}
        />
      </View>
      <View>
        <FlatList
          onEndReachedThreshold={0.7}
          horizontal={true}
          keyExtractor={(item, index) => index.toString()}
          data={props.flter_collection || []}
          ListEmptyComponent={showEmptyListView()}
          renderItem={({ item }) => eachExplore(item)}
        />
      </View>
      <View>
        <Text
          style={{
            marginTop: scale(10),
            fontSize: scale(16),
            marginBottom: scale(10)
          }}>
          Hot Deals
        </Text>
        <FlatList
          onEndReachedThreshold={0.7}
          horizontal={true}
          keyExtractor={(item, index) => index.toString()}
          data={props.treding_Deals || []}
          renderItem={({ item }) => eachTred(item)}
        />
      </View>
    </View>
  );
};
// export default ExploreScreen;

function mapStateToProps(state) {
  return {
    top_seller_list: state.home.top_seller_list,
    popular_collection_list: state.home.popular_collection_list,
    category_list: state.home.category_list,
    flter_collection: state.home.flter_collection,
    item_list: state.home.item_list,
    treding_Deals: state.home.treding_Deals,

  };
}

export default connect(mapStateToProps, { getfilterCollection, getfilterCollectionParticularList, getNftDetails })(ExploreScreen);
