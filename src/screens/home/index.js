/* eslint-disable react-native/no-color-literals */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect } from 'react';
import { FlatList, StyleSheet, Platform, Text, View } from 'react-native';
import { connect } from 'react-redux';
import { scale } from '../../components/Scale';

import {
  CategoryList,
  TopSellerComponent,
  TrendingList,
} from '../../components/Homehelper/helper';
import {
  getCategoryList,
  getfilterCollection,
  getList,
  getPopularCollection,
  getTopSheller,
  getTredingList,
  ethToUSDConvertor,
  getNftDetails
} from '../../redux/action/home';
import { useIsFocused } from '@react-navigation/native';
const HomeScreen = props => {
  const isFocused = useIsFocused();

  useEffect(() => {
    props.getTopSheller();
    props.getPopularCollection();
    props.getCategoryList();
    props.getfilterCollection();
    props.getList();
    props.getTredingList();
    props.ethToUSDConvertor();
  }, []);
  const eachCat = each => {
    // console.log('eachCat ' + JSON.stringify(each));
    return (
      <View>
        <CategoryList item={each} />
      </View>
    );
  };
  const eachTrending = each => {
    // console.log('eachCat ' + JSON.stringify(each));
    return (
      <View>
        <TrendingList item={each}
          onChange={onChangeTrend} />
      </View>
    );
  };
  const onChangeTrend = (item) => {
    // console.log('onChangeTrend item' + JSON.stringify(item))
    props.getNftDetails(item.id)
    props.navigation.navigate('ViewNft', { nftDetails: item })
  }
  const eachItem = each => {
    return (
      <View style={{ padding: 10 }}>
        <TopSellerComponent item={each.item} />
      </View>
    );
  };
  // console.log('props.treding_Deals111 '+ JSON.stringify(props.treding_Deals))
  // console.log('props.top_seller_list 11'+ JSON.stringify(props.top_seller_list))

  return (
    <View style={styles.pageContainer}>
      {/* <View style={{ flex: 0.13 }}>
        <FlatList
          onEndReachedThreshold={0.7}
          horizontal={true}
          keyExtractor={(item, index) => index.toString()}
          data={props.category_list || []}
          renderItem={({ item }) => eachCat(item)}
        />
      </View> */}
      {/* <View style={{ flex: 0.6, }}> */}
        <Text
          style={{
            fontSize: scale(16),
            fontWeight: '600',
            marginBottom: scale(10)
          }}>
          Trending Deals
        </Text>
        <FlatList
          onEndReachedThreshold={0.7}
          horizontal={true}
          keyExtractor={(item, index) => index.toString()}
          data={props.treding_Deals || []}
          renderItem={({ item }) => eachTrending(item)}
        />
      {/* </View> */}
      {/* <View style={{ flex: 0.5, }}> */}
        <Text
          style={{
            fontSize: scale(16),
            fontWeight: '600',
            marginBottom: scale(10),
            marginTop: scale(10)

          }}>
          Top Sellers
        </Text>
        <FlatList
          onEndReachedThreshold={0.7}
          style={{ width: '100%' }}
          keyExtractor={(item, index) => index.toString()}
          data={props.top_seller_list || []}
          //   refreshControl={<RefreshControl refreshing={isRefreshing} onRefresh={handleRefresh} />}
          renderItem={({ item }) => eachItem(item)}
        />
      {/* </View> */}
    </View>
  );
};

const styles = StyleSheet.create({
  pageContainer: {
    margin: 10, marginTop: Platform.OS === 'ios' ? scale(40) : 0
  },
});

// export default HomeScreen;
function mapStateToProps(state) {
  return {
    top_seller_list: state.home.top_seller_list,
    popular_collection_list: state.home.popular_collection_list,
    category_list: state.home.category_list,
    flter_collection: state.home.flter_collection,
    item_list: state.home.item_list,
    treding_Deals: state.home.treding_Deals,

  };
}

export default connect(mapStateToProps, {
  getCategoryList,
  getList,
  getPopularCollection,
  getTopSheller,
  getfilterCollection,
  getTredingList,
  ethToUSDConvertor,
  getNftDetails
})(HomeScreen);
