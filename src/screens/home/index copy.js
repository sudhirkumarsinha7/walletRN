// import * as React from 'react';
// import { View, Text,FlatList,Image,Animated } from 'react-native';
// import  {imageslist,DeviceWidth,DeviceHeight} from '../../config/global'
// import { SwipeListView } from 'react-native-swipe-list-view';
// import ViewOverflow from 'react-native-view-overflow';
// // import Swiper from 'react-native-deck-swiper'
// import styles from './style'

// const HomeScreen=() =>{
//   const useSwiper = useRef(null).current
//   const handleOnSwipedLeft = () => useSwiper.swipeLeft()
//   const handleOnSwipedTop = () => useSwiper.swipeTop()
//   const handleOnSwipedRight = () => useSwiper.swipeRight()
//   const eachItem = (item) => {
//     return <ViewOverflow>
//     <View style={{padding:10}}>
//                                   <Image
//                                 style={{
//                                     width: DeviceWidth, height: DeviceWidth,
//                                 }}
//                                 source={item.image}
//                                 resizeMode="contain"
//                             />
//     </View>
//     </ViewOverflow>
// }
//   return (
//     <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
//       <Text>Home Screen</Text>
//       <FlatList
//                     onEndReachedThreshold={0.7}
//                     style={{ width: '100%' }}
//                     keyExtractor={(item, index) => index.toString()}
//                     data={imageslist}
//                     //   refreshControl={<RefreshControl refreshing={isRefreshing} onRefresh={handleRefresh} />} 
//                     renderItem={({ item }) => eachItem(item)}
//                 />
//                  <Text>Home Screen</Text>
//       {/* <SwipeListView
//                     onEndReachedThreshold={0.7}
//                     style={{ width: '100%' }}
//                     keyExtractor={(item, index) => index.toString()}
//                     data={imageslist}
//                     //   refreshControl={<RefreshControl refreshing={isRefreshing} onRefresh={handleRefresh} />} 
//                     renderItem={({ item }) => eachItem(item)}
//                     renderHiddenItem={ (data, rowMap) => (
//                       <View style={{}}>
//                           <Text>Left</Text>
//                           <Text>Right</Text>
//                       </View>
//                   )}
//                   leftOpenValue={75}
//                   rightOpenValue={-75}
//                 /> */}
//     </View>
//   );
// }
// export default HomeScreen;
import React,{useEffect} from 'react';
import {View, StyleSheet} from 'react-native';
import Card from '../../components/Card';
import users from '../../assets/users';

import AnimatedStack from '../../components/AnimatedStack';
import { connect } from 'react-redux';
import {getTopSheller,getPopularCollection,getCategoryList,getfilterCollection,getList} from '../../redux/action/home'
const HomeScreen = (props) => {
  useEffect(() => {
  props.getTopSheller();
  props.getPopularCollection();
  props.getCategoryList();
  props.getfilterCollection();
  props.getList();

  }, []);

  const onSwipeLeft = user => {
    console.warn('swipe left', user.name);
  };

  const onSwipeRight = user => {
    console.warn('swipe right: ', user.name);
  };
console.log('top_seller_list top_seller_list '+ JSON.stringify(props.top_seller_list))
  return (
    <View style={styles.pageContainer}>
      <AnimatedStack
        data={users}
        renderItem={({item}) => <Card user={item} />}
        onSwipeLeft={onSwipeLeft}
        onSwipeRight={onSwipeRight}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  pageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    width: '100%',
  },
});

// export default HomeScreen;
function mapStateToProps(state) {
  return {
    top_seller_list: state.home.top_seller_list,
    popular_collection_list: state.home.popular_collection_list,
    category_list: state.home.category_list,
    flter_collection: state.home.flter_collection,
    item_list:state.home.item_list,
  };
}

export default connect(mapStateToProps, { getTopSheller, getPopularCollection,getCategoryList,getfilterCollection,getList })(HomeScreen);