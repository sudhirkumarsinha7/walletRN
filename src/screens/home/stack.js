import * as React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Home from './index';
import Screen1 from './screen1';
import ViewNft from '../explore/viewnft';

const Stack = createNativeStackNavigator();
const HomeStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={Home}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="ViewNft"
        component={ViewNft}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="HomeScreen1"
        component={Screen1}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
};
export default HomeStack;
