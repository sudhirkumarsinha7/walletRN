/**
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState } from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  Image,
  View,
  Button,
  Alert,
  TouchableOpacity,
  TextInput,
} from 'react-native';

import AsyncStorage from "@react-native-async-storage/async-storage";
import { connect } from 'react-redux';
import ProfileDetials from '../../components/Profilehelper/ProfileDetails'
import { scale } from '../../components/Scale';

import WalletConnectProvider from '@walletconnect/web3-provider';
import { useWalletConnect, withWalletConnect } from '@walletconnect/react-native-dapp';
import { NavigationContainer, useIsFocused } from '@react-navigation/native';
import { ethers } from 'ethers';
import {
  LogIn,
  Register
} from '../../redux/action/auth';
import {
  setWalletAddress
} from '../../redux/action/user';
import { Colors, DeviceHeight } from '../../components/Common/Style';
import WalletLogin from '../../components/Profilehelper/WalletLogin'
const ProfileScreen = (props) => {
  const isFocused = useIsFocused();
  const connector = useWalletConnect(); // valid
  const [connected, setconnected] = useState(false)
  const [walletAddress, SetWalletAddress] = useState('')
  useEffect(() => {
    if (connector && connector.accounts.length && connector.accounts[0]) {
      setconnected(true)
    }
  }, [isFocused]);

  const onConnect = async () => {
    connector.connect()
    const provider = new WalletConnectProvider({
      bridge: "https://bridge.walletconnect.org",
      rpc: { 1: "https://rpc.ankr.com/eth", 3: "https://ropsten.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161" },
      infuraId: "8043bb2cf99347b1bfadfb233c5325c0",
      connector: connector,
      connected: true,
      qrcode: false,
    });
    await provider.enable();
    const ethers_provider = new ethers.providers.Web3Provider(provider);
    setWalletAddress(connector.accounts[0])
    setconnected(true)
  }


  // console.log('connector ' + JSON.stringify(connector.accounts[0]))
  // return <View >
  //     <WalletLogin />
  //   </View>
  if (connector && connector.accounts.length && connector.accounts[0]) {
    return <View >
      <ProfileDetials />
    </View>
  }
  return <View style={{ backgroundColor: Colors.whiteFF }}>
    <View style={{ margin: 10, marginTop: Platform.OS === 'ios' ? scale(40) : 0 }}>
      <View style={{
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: scale(50)
      }}>
        <Image
          style={{
            width: 200,
            height: 200,
            borderRadius: 5,
            backgroundColor: 'green',
            marginBottom: scale(20)
          }}
          source={require('../../assets/wallet.png')}
        />
        <Text style={{ fontSize: scale(20), fontWeight: 'bold', color: Colors.blueFc }}>Login into your Wallet</Text>
        <Text style={{ fontSize: scale(16), marginTop: scale(20) }}>Connect to any WalletConnect supported wallet to securely store/buy/sell your digital goods</Text>

      </View>

      <TouchableOpacity onPress={() => onConnect()} style={{ backgroundColor: Colors.blueFF, padding: 10, borderRadius: scale(30), marginTop: scale(30), margin: 10 }}>
        <Text style={{ fontSize: scale(18), color: Colors.whiteF7, textAlign: 'center', fontWeight: 'bold' }}>Connect Wallet</Text>

      </TouchableOpacity>
      <Text
        style={{
          alignSelf: 'center',
          color: '#0093E9',
          fontWeight: 'bold',
          margin: 10
        }}>
        {'OR'}
      </Text>

      <Text
        style={{
          // color: '#0093E9',
          marginLeft:scale(12)
        }}>
        {'Manual login View Only'}
      </Text>
      <View style={{}}>
        <TextInput
          value={walletAddress}
          placeholder={'Enter Wallet Address'}
          style={{ color: Colors.blueFF, borderWidth: 1, borderColor: Colors.blueFF, padding: 10, borderRadius: scale(30), margin: 10, fontSize: 16 }}
          onChange={(text) => { SetWalletAddress(text) }}
        />

        <TouchableOpacity onPress={() => onConnect()} style={{ backgroundColor: Colors.blueFF, padding: 10, borderRadius: scale(30), marginTop: scale(30), margin: 10 }}>
          <Text style={{ fontSize: scale(18), color: Colors.whiteF7, textAlign: 'center', fontWeight: 'bold' }}>Log in</Text>

        </TouchableOpacity>
      </View>
    </View>
  </View>

};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

function mapStateToProps(state) {
  return {
    top_seller_list: state.home.top_seller_list,
    popular_collection_list: state.home.popular_collection_list,
    category_list: state.home.category_list,
    flter_collection: state.home.flter_collection,
    item_list: state.home.item_list,
    treding_Deals: state.home.treding_Deals,
    walletAddress:state.user.walletAddress
  };
}
export default connect(mapStateToProps, {
  LogIn,
  Register
})(ProfileScreen);

