import * as React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import More from './index';
import Screen1 from './screen1';
const Stack = createNativeStackNavigator();
const Moretack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="More"
        component={More}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="MoreScreen1"
        component={Screen1}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
};
export default Moretack;
