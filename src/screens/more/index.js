/**
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState } from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Button,
  Alert
} from 'react-native';

import AsyncStorage from "@react-native-async-storage/async-storage";
import { connect } from 'react-redux';

import WalletConnectProvider from '@walletconnect/web3-provider';
import { useWalletConnect, withWalletConnect } from '@walletconnect/react-native-dapp';
import { DEV_URL } from '../../config/config'
import axios from 'axios';
import { ethers } from 'ethers';
import {
  LogIn,
  Register
} from '../../redux/action/auth';
const abi = require("./abi.json");
const contractaddress = "0x05d2259Eb45309De47C267638721Cd91e7470680";

const LogInScreen = (props) => {
  const connector = useWalletConnect(); // valid
  const [connected, setconnected] = useState(false)

  const sendTx = async (contract, functionname, input) => {
    console.log("sendTx Sending tx to set", input);
    const tx = await contract[functionname](...input);
    const txresponse = await tx.wait();
    const blkno = txresponse.blockNumber ? txresponse.blockNumber : 0;
    const txnevnts = txresponse.events ? txresponse.events : "";
    let obj = { txHash: txresponse.transactionHash, blockNumber: blkno, event: txnevnts }
    console.log("sendTx,Responses", obj);
    return obj;
  }

  const onConnect = async () => {
    connector.connect()

    const provider = new WalletConnectProvider({
      bridge: "https://bridge.walletconnect.org",
      rpc: { 1: "https://rpc.ankr.com/eth", 3: "https://ropsten.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161" },
      infuraId: "8043bb2cf99347b1bfadfb233c5325c0",
      connector: connector,
      connected: true,
      qrcode: false,
    });
    await provider.enable();
    //  console.log("connector", connector)
    const ethers_provider = new ethers.providers.Web3Provider(provider);
    const signer = await ethers_provider.getSigner();
    console.log("signer", signer)
    const userNetwork = await ethers_provider.getNetwork();
    console.log("userNetwork", userNetwork)

    console.log("addresss", await signer.getAddress())

    console.log("accounts22", signer.provider.provider.accounts[0])
    const balance = await ethers_provider.getBalance(signer.provider.provider.accounts[0]);
    console.log("balance", balance)
    const instance = new ethers.Contract(contractaddress, abi, ethers_provider);
    console.log("INtsance is", instance);
    const c = ethers.utils.parseUnits("1", 'ether')
    //  const b = await sendTx(instance['connect'](signer), "load", [c])
    //  console.log("B", b)
    //  const account = await signer.getAccounts();


    setconnected(true)
    Alert.alert( 'Success', 'User updated successfully',
                    [
                      {text: 'Ok', onPress: () =>verifyAuth()},
                    ],
                    { cancelable: false }
                  );
  }
  const verifyAuth = async () => {
    // const message = "\x19Ethereum Signed Message:\nAn amazing message, for use with MetaMask!";
    const message = "An amazing message, for use with MetaMask!";

    let signatures;
    const provider = new WalletConnectProvider({
      bridge: "https://bridge.walletconnect.org",
      rpc: { 1: "https://rpc.ankr.com/eth", 3: "https://ropsten.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161" },
      infuraId: "8043bb2cf99347b1bfadfb233c5325c0",
      connector: connector,
      connected: true,
      qrcode: false,
    });
    await provider.enable();
    const ethers_provider = new ethers.providers.Web3Provider(provider);
    const signer = await ethers_provider.getSigner();

    signatures = await signer.signMessage(message);
    console.log('signatures ', signatures);
    let RegisterData = {
      walletAddress: connector.accounts[0],
    }
    let LoginData = {
      walletId: connector.accounts[0],
      signature: signatures,
      message: message,
    }
    await props.Register(RegisterData);
    await props.LogIn(LoginData)

  };

  const onDisConnect = () => {
    connector.killSession()
    setconnected(false)
  }
  console.log('connector ' + JSON.stringify(connector))
  if (!connected) {
    return <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <Button title="Connect" onPress={() => onConnect()} />

    </View>
  }
  return <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
    <Button title="Kill Session" onPress={() => onDisConnect()} />
    <Text>Connected Wallet:{connector.accounts[0]}</Text>
    <Button title="verifyAuth" onPress={() => verifyAuth()} />
    <Button title="Connect" onPress={() => onConnect()} />

  </View>


};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

function mapStateToProps(state) {
  return {
    top_seller_list: state.home.top_seller_list,
    popular_collection_list: state.home.popular_collection_list,
    category_list: state.home.category_list,
    flter_collection: state.home.flter_collection,
    item_list: state.home.item_list,
    treding_Deals: state.home.treding_Deals,

  };
}
export default connect(mapStateToProps, {
  LogIn,
  Register
})(LogInScreen);

// export default withWalletConnect(LogInScreen, {
//   clientMeta: {
//     description: "Connect with WalletConnect",
//   },
//   redirectUrl:
//     Platform.OS === "web" ? window.location.origin : "yourappscheme://",
//   storageOptions: {
//     asyncStorage: AsyncStorage,
//   },
// });
