import * as React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Search from './index';
import Screen1 from './screen1';
const Stack = createNativeStackNavigator();
const SearchStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Search"
        component={Search}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="SearchScreen1"
        component={Screen1}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
};
export default SearchStack;
