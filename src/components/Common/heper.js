import React, { useEffect, useState, useRef } from 'react';
import { Button, FlatList, Image, SafeAreaView, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View, Platform, } from 'react-native';

import { Colors, DeviceWidth, CommonStyle } from '../Common/Style';
import { moderateScale, scale, verticalScale } from "../Scale";
import Video from 'react-native-video';
import MediaControls, { PLAYER_STATES } from 'react-native-media-controls';

const imageStyle = { width: DeviceWidth / 2.5, height: 230, borderRadius: 10 }
const imageStyle1 = { width: 1, height: 1, borderRadius: 10 }
const ViewimageStyle = { width: DeviceWidth / 1.2, height: 600, borderRadius: 10 }
const pausedStyple = { width: 30, height: 40, borderRadius: 10 }

export const VideoComponent = props => {
    //'https://assets.mixkit.co/videos/download/mixkit-countryside-meadow-4075.mp4',
    const videoPlayer = useRef(null);
    const [currentTime, setCurrentTime] = useState(0);
    const [duration, setDuration] = useState(0);
    const [isFullScreen, setIsFullScreen] = useState(false);
    const [isLoading, setIsLoading] = useState(true);
    const [paused, setPaused] = useState(true);
    const [
        playerState, setPlayerState
    ] = useState(PLAYER_STATES.PLAYING);
    const [screenType, setScreenType] = useState('content');

    const onSeek = (seek) => {
        videoPlayer.current.seek(seek);
    };

    const onPaused = (playerState) => {
        //Handler for Video Pause
        setPaused(!paused);
        setPlayerState(playerState);
    };

    const onReplay = () => {
        //Handler for Replay
        setPlayerState(PLAYER_STATES.PLAYING);
        videoPlayer.current.seek(0);
    };

    const onProgress = (data) => {
        // Video Player will progress continue even if it ends
        if (!isLoading && playerState !== PLAYER_STATES.ENDED) {
            setCurrentTime(data.currentTime);
        }
    };

    const onLoad = (data) => {
        setDuration(data.duration);
        setIsLoading(false);
    };

    const onLoadStart = (data) => setIsLoading(true);

    const onEnd = () => setPlayerState(PLAYER_STATES.ENDED);

    const onError = () => alert('Oh! ', error);

    const exitFullScreen = () => {
        alert('Exit full screen');
    };

    const enterFullScreen = () => { };

    const onFullScreen = () => {
        setIsFullScreen(isFullScreen);
        if (screenType == 'content') setScreenType('cover');
        else setScreenType('content');
    };

    const renderToolbar = () => (
        <View>
            <Text style={styles.toolbar}> toolbar </Text>
        </View>
    );

    const onSeeking = (currentTime) => setCurrentTime(currentTime);

    return (
        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
            {isLoading ?
                <Image
                    source={require('../../assets/circle-loader.gif')}
                    style={[props.imageStyle || imageStyle]}
                />
                : null}
            <Video
                onEnd={onEnd}
                onLoad={onLoad}
                onLoadStart={onLoadStart}
                onProgress={onProgress}
                paused={paused}
                ref={videoPlayer}
                resizeMode={screenType}
                onFullScreen={isFullScreen}
                source={{ uri: props.url }}
                // style={styles.mediaPlayer}

                style={[
                    isLoading ? imageStyle1 : props.imageStyle || imageStyle,
                ]}
                volume={10}
            />
            <MediaControls
                duration={duration}
                isLoading={isLoading}
                mainColor="#333"
                onFullScreen={onFullScreen}
                onPaused={onPaused}
                onReplay={onReplay}
                onSeek={onSeek}
                onSeeking={onSeeking}
                playerState={playerState}
                progress={currentTime}
                toolbar={renderToolbar()}
            />
            {/* {paused?<Image
        source={require('../../assets/circle-loader.gif')}
        style={pausedStyple}
      />:null} */}

        </View>
    );
};
export const ImageComponent = props => {

    const [isLoading, setIsLoading] = useState(true);

    return (
        <View>
            {isLoading ? <View
                style={{ justifyContent: 'center', alignItems: 'center' }}>
                <Image
                    source={require('../../assets/circle-loader.gif')}
                    style={[props.imageStyle || imageStyle]}
                />
            </View> : null}
            <Image
                source={{ uri: props.url }}
                onLoadEnd={() => setIsLoading(false)}
                style={[
                    isLoading ? imageStyle1 : props.imageStyle || imageStyle,
                ]}
            />
        </View>
    );
};
export const InputTextHelper = (props) => {

    const { eachRow, state, Form, edit = true, image = '', updateState } = props;
    return (
        <View >
            <View style={{
                marginTop: scale(10),
                marginBottom: scale(10),
            }}>
                <Text style={{
                    fontSize: scale(14),
                }}>{eachRow.displayName}</Text>
            </View>
            <View style={{
                backgroundColor: Colors.whiteFF
            }}>
                <TextInput
                    style={{
                        borderWidth: 0.5,
                        padding: scale(10),
                        borderColor: Colors.black70,
                        borderRadius: scale(5),

                    }}
                    placeholder={'Enter ' + eachRow.displayName}
                    keyboardType={eachRow.keyboardType}
                    value={
                        state[eachRow.stateName]
                            ? state[eachRow.stateName] + ''
                            : state[eachRow.stateName]
                    }
                    onChangeText={updateState}
                    editable={edit}
                    multiline={true}
                />
            </View>
        </View>
    );
}

export const InputTextHelper1 = (props) => {

    const { eachRow, state, updateState, edit = true } = props;
    return (
        <View
            style={{
                margin: verticalScale(5),
                marginLeft: 10,
                marginRight: 10,
            }}>
            <Text
                style={{
                    fontFamily: 'Roboto-Regular',
                    fontWeight: 'bold',
                    color: Colors.black0,
                }}>
                {eachRow.displayName}
            </Text>
            <View
                style={{
                    width: '100%',
                    borderBottomColor: '#E8E8E8',
                }}>
                <TextInput
                    style={{
                        borderBottomWidth: edit ? 1 : 0,
                        borderBottomColor: '#E8E8E8',
                    }}
                    placeholder={'Enter ' + eachRow.displayName}
                    keyboardType={eachRow.keyboardType}
                    value={
                        state[eachRow.stateName]
                            ? state[eachRow.stateName] + ''
                            : state[eachRow.stateName]
                    }
                    onChangeText={text => updateState({ [eachRow.stateName]: text })}
                    editable={edit}
                    multiline={true}
                />
            </View>
        </View>
    );
}
export const CustomTextView = (props) => {

    const { leftText, rightText } = props;
    return (
        <View style={{ ...CommonStyle.dropdownView }}>
            <View style={{ ...CommonStyle.dropdownleftView }}>
                <Text
                    style={{
                        fontFamily: 'Roboto-Regular',
                        color: Colors.black70,
                        fontSize: scale(12),
                    }}>
                    {leftText}
                </Text>
            </View>
            <View
                style={{
                    width: '50%',
                    borderBottomColor: '#E8E8E8',
                }}>
                <Text
                    style={{
                        fontFamily: 'Roboto-Regular',
                        fontSize: scale(12),
                        // fontWeight: 'bold',
                        color: '#000000',
                    }}
                    numberOfLines={3}
                    ellipsizeMode={'tail'}>
                    {rightText ? rightText + '' : ''}
                </Text>
            </View>
        </View>
    );
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    toolbar: {
        marginTop: 30,
        backgroundColor: 'gray',
        padding: 10,
        borderRadius: 5,
    },
    mediaPlayer: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        backgroundColor: 'black',
        justifyContent: 'center',
    },
});