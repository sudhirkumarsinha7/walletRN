import React, { Component } from 'react';
import { FlatList, Button, View, Text, Image, Dimensions, TouchableOpacity, ScrollView, SafeAreaView, StyleSheet } from 'react-native';
import { scale, moderateScale, verticalScale } from "../Scale";
import { Colors } from '../Common/Style';
import Modal from 'react-native-modal';
const PopUp = (props) => {
    return (
        <Modal
            isVisible={props.isModalVisible}
            style={{
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 20,
            }}
            animationInTiming={1000}
            animationOutTiming={1000}
            backdropTransitionInTiming={1000}
            backdropTransitionOutTiming={1000}>
            <View style={{ width: scale(250), height: scale(180), backgroundColor: "#ffffff", borderRadius: 20, justifyContent: "center", alignItems: "center" }}>
                <Text style={{ fontSize: scale(20), textAlign: 'center', }}>{'Verify Account'}</Text>
                <Text style={{ fontSize: scale(15), textAlign: 'center', marginTop: verticalScale(30), color: "#707070", marginRight: scale(20), marginLeft: scale(20) }}>{'Verify your Account?'}</Text>
                <View style={{ width: scale(230), marginTop: verticalScale(30),  }}>
                    

                    <TouchableOpacity
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderRadius: 10,
                            // width: scale(105),
                            backgroundColor:  Colors.blueFF,
                            padding:10
                            // height: scale(35),
                        }}
                        onPress={props.Verify}>
                        <Text style={{ color: Colors.whiteFF, fontSize: scale(15) }}>{'Verify'}</Text>
                    </TouchableOpacity>

                </View>
            </View>
        </Modal>
    );
}

export default (PopUp);