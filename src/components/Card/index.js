/* eslint-disable no-undef */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react-native/no-color-literals */
import React from 'react';
import {Image, ImageBackground, StyleSheet, Text,View} from 'react-native';

import { moderateScale, scale, verticalScale } from '../Scale';

const Card = props => {
  const {name, image, bio} = props.user;
  return (
    <View style={styles.card}>
      <ImageBackground
        source={{
          uri: image,
        }}
        style={styles.image}>
        <View style={styles.cardInner}>
          <Text style={styles.name}>{name}</Text>
          <Text style={styles.bio}>{bio}</Text>
        </View>
      </ImageBackground>
    </View>
  );
};
export const Empty_Card = (props) => {

  return (
      <View
          style={{
              marginTop: verticalScale(20),
              backgroundColor: '#FFFFFF',
              borderRadius: 8,
              // width: scale(328),
              margin: 15,
              alignItems: 'center',
              justifyContent: 'center',
              height: scale(221),
          }}>
          {/* <Text
              style={{ fontSize: scale(20), textAlign: 'center' }}
              numberOfLines={2}>
              {props.isYou ? 'Looks like' : 'Looks like your'}
          </Text> */}
          <Text
              style={{ fontSize: scale(20), width: scale(190), textAlign: 'center' }}
              numberOfLines={2}>
              {props.Text}
          </Text>
          <Image
              style={{
                  width: scale(260),
                  height: scale(171),
                  marginTop: verticalScale(11),
              }}
              resizeMode="cover"
              source={require('../../assets/Img/Empty.png')}
          />
      </View>
  );
}

const styles = StyleSheet.create({
  card: {
    width: '100%',
    height: '100%',
    borderRadius: 10,
    backgroundColor: '#fefefe',

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.36,
    shadowRadius: 6.68,

    elevation: 11,
  },
  image: {
    width: '100%',
    height: '100%',
    borderRadius: 10,
    overflow: 'hidden',

    justifyContent: 'flex-end',
  },
  cardInner: {
    padding: 10,
  },
  name: {
    fontSize: 30,
    color: 'white',
    fontWeight: 'bold',
  },
  bio: {
    fontSize: 18,
    color: 'white',
    lineHeight: 25,
  },
});

export default Card;