import React, { Component } from 'react';
import { FlatList, Button, View, Text, Image, Dimensions, TouchableOpacity, ScrollView, SafeAreaView, StyleSheet } from 'react-native';
import { scale, moderateScale, verticalScale } from "../Scale";
import { Colors } from '../Common/Style';
import Modal from 'react-native-modal';
const PopUp = (props) => {
    return (
        <Modal
            isVisible={props.isModalVisible}
            style={{
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 20,
            }}
            animationInTiming={1000}
            animationOutTiming={1000}
            backdropTransitionInTiming={1000}
            backdropTransitionOutTiming={1000}>
            <View style={{ width: scale(250), height: scale(220), backgroundColor: "#ffffff", borderRadius: 20, justifyContent: "center", alignItems: "center" }}>
                <Text style={{ fontSize: scale(20), textAlign: 'center', }}>{'Delete Account'}</Text>
                <Text style={{ fontSize: scale(15), textAlign: 'center', marginTop: verticalScale(50), color: "#707070", marginRight: scale(20), marginLeft: scale(20) }}>{'Are you sure you want to Delete Account?'}</Text>
                <View style={{ width: scale(230), flexDirection: "row", marginTop: verticalScale(50), alignItems: "center", justifyContent: "space-between" }}>
                    <TouchableOpacity
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderRadius: 10,
                            width: scale(105),
                            height: scale(35),
                            backgroundColor:Colors.blueFc
                        }}
                        onPress={props.Cancel}>
                     
                            <Text style={{ color: '#FFFFFF', fontSize: scale(15) }}>{'Cancel'}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderRadius: 10,
                            width: scale(105),
                            backgroundColor:  Colors.red00,
                            height: scale(35),
                        }}
                        onPress={props.Logout}>
                        <Text style={{ color: Colors.whiteFF, fontSize: scale(15) }}>{'Delete'}</Text>
                    </TouchableOpacity>

                </View>
            </View>
        </Modal>
    );
}

export default (PopUp);