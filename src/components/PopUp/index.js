/* eslint-disable react-native/no-color-literals */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable sort-keys */
import React, { Component } from 'react';
import { Button, FlatList, Image, SafeAreaView, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View, Platform, } from 'react-native';
import Modal from 'react-native-modal';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { Colors, DeviceWidth } from '../Common/Style';
import { moderateScale, scale, verticalScale } from "../Scale";
// export const PopUp2 = (props) => {

//     return (
//         <Modal
//             isVisible={props.isModalVisible}
//             style={{
//                 justifyContent: 'center',
//                 alignItems: 'center',
//                 borderRadius: 20,
//             }}
//             animationInTiming={1000}
//             animationOutTiming={1000}
//             backdropTransitionInTiming={1000}
//             backdropTransitionOutTiming={1000}>
//             <View style={{ width: scale(250), height: scale(250), backgroundColor: "#ffffff", borderRadius: 20, justifyContent: "center", alignItems: "center" }}>
//                 <Image
//                     style={{ width: scale(46.16), height: scale(46.16), }}
//                     source={props.image}
//                     resizeMode='contain'
//                 />


//                 <Text style={{ fontSize: scale(15), textAlign: 'center', marginTop: verticalScale(20), marginBottom: verticalScale(20), color: "#707070", marginRight: scale(27), marginLeft: scale(27) }}>{props.text1}</Text>

//                 <View style={{ flexDirection: 'row' }}>
//                     <TouchableOpacity
//                         style={{
//                             justifyContent: 'center',
//                             alignItems: 'center',
//                             width: scale(90),
//                             padding: 10,
//                             borderRadius: 10,
//                             // backgroundColor: Colors.blueFc,
//                             borderWidth: 1,
//                         }}
//                         onPress={props.cancel}>
//                         <Text style={{ color: Colors.blueFc, fontSize: scale(15) }}>{props.label1}</Text>
//                     </TouchableOpacity>
//                     <TouchableOpacity
//                         style={{
//                             justifyContent: 'center',
//                             alignItems: 'center',
//                             width: scale(90),
//                             marginLeft: scale(10),
//                             padding: 10,
//                             borderRadius: 10,
//                             backgroundColor: Colors.blueFc,
//                             borderWidth: 1,
//                         }}
//                         onPress={props.Ok}>
//                         <Text style={{ color: '#FFFFFF', fontSize: scale(15) }}>{props.label}</Text>
//                     </TouchableOpacity>
//                 </View>

//             </View>
//         </Modal>
//     )
// }

// export const PopUp1 = (props) => {

//     return (
//         <Modal
//             isVisible={props.isModalVisible}
//             style={{
//                 justifyContent: 'center',
//                 alignItems: 'center',
//                 borderRadius: 20,
//             }}
//             animationInTiming={1000}
//             animationOutTiming={1000}
//             backdropTransitionInTiming={1000}
//             backdropTransitionOutTiming={1000}>
//             <View style={{ width: scale(300), height: scale(200), backgroundColor: "#ffffff", borderRadius: 20, justifyContent: "center", alignItems: "center" }}>

//                 <Image
//                     style={{ width: scale(30.16), height: scale(30.16), tintColor: Colors.blueFc }}
//                     source={props.image}
//                     resizeMode='contain'
//                 />

//                 <Text style={{ fontSize: scale(15), textAlign: 'center', marginTop: verticalScale(10), color: Colors.blueFc, marginRight: scale(15), marginLeft: scale(15), fontWeight: 'bold' }}>{props.text2}</Text>

//                 <View style={{ flexDirection: 'row', marginTop: verticalScale(20), justifyContent: 'space-between' }}>
//                     {/* <View style={{ flex: 0.4, }}> */}
//                     <TouchableOpacity
//                         style={{
//                             justifyContent: 'center',
//                             alignItems: 'center',
//                             width: scale(90),
//                             padding: 10,
//                             borderRadius: 10,
//                             borderColor: Colors.blueFc,
//                             borderWidth: 1,
//                         }}
//                         onPress={props.cancel}>

//                         <Text style={{ color: Colors.backgroundBlue, fontSize: scale(12) }}>{props.label1}</Text>

//                     </TouchableOpacity>

//                     <TouchableOpacity
//                         style={{
//                             justifyContent: 'center',
//                             alignItems: 'center',
//                             // borderRadius: 10,
//                             width: scale(90),
//                             padding: 10,
//                             borderRadius: 10,
//                             backgroundColor: Colors.yellow3B,
//                             marginLeft: 10

//                         }}
//                         onPress={props.Ok}>

//                         <Text style={{ color: '#FFFFFF', fontSize: scale(12) }}>{props.label}</Text>
//                     </TouchableOpacity>
//                     {/* </View> */}
//                 </View>
//             </View>
//         </Modal>
//     );
// }

// export const Logout_PopUp = (props) => {

//     return (
//         <Modal
//             isVisible={props.isModalVisible}
//             style={{
//                 justifyContent: 'center',
//                 alignItems: 'center',
//                 borderRadius: 20,
//             }}
//             animationInTiming={1000}
//             animationOutTiming={1000}
//             backdropTransitionInTiming={1000}
//             backdropTransitionOutTiming={1000}>
//             <View style={{ width: scale(250), height: scale(220), backgroundColor: "#ffffff", borderRadius: 20, justifyContent: "center", alignItems: "center" }}>

//                 <Text style={{ fontSize: scale(20), textAlign: 'center', }}>Log Out</Text>

//                 <Text style={{ fontSize: scale(15), textAlign: 'center', marginTop: verticalScale(50), color: "#707070", marginRight: scale(20), marginLeft: scale(20) }}>Are you sure you want to logout?</Text>

//                 <View style={{ width: scale(230), flexDirection: "row", marginTop: verticalScale(50), alignItems: "center", justifyContent: "space-between" }}>

//                     <TouchableOpacity
//                         style={{
//                             justifyContent: 'center',
//                             alignItems: 'center',
//                             borderRadius: 10,
//                             width: scale(105),
//                             height: scale(35),
//                         }}
//                         onPress={props.Cancel}>
//                         <Text style={{ color: '#FFFFFF', fontSize: scale(15) }}>Cancel</Text>

//                     </TouchableOpacity>

//                     <TouchableOpacity
//                         style={{
//                             justifyContent: 'center',
//                             alignItems: 'center',
//                             borderRadius: 10,
//                             width: scale(105),
//                             borderColor: Colors.blueFc,
//                             backgroundColor: "#FFFFFF",
//                             borderWidth: 1,
//                             height: scale(35),
//                         }}
//                         onPress={props.Logout}>

//                         <Text style={{ color: '#0093E9', fontSize: scale(15) }}>Log out</Text>

//                     </TouchableOpacity>

//                 </View>
//             </View>
//         </Modal >
//     )
// }

export const PopUp = (props) => {

    return (
        <Modal
            isVisible={props.isModalVisible}
            style={{
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 20,
            }}
            animationInTiming={1000}
            animationOutTiming={1000}
            backdropTransitionInTiming={1000}
            backdropTransitionOutTiming={1000}>
            <View style={{ width: DeviceWidth / 1.2, height: scale(250), backgroundColor: "#ffffff", borderRadius: 20, justifyContent: "center", alignItems: "center" }}>
                <Image
                    style={{ width: scale(46.16), height: scale(46.16), }}
                    source={props.image}
                    resizeMode='contain'
                />


                <Text style={{ fontSize: scale(15), textAlign: 'center', marginTop: verticalScale(20), marginBottom: verticalScale(20), color: "#707070", marginRight: scale(27), marginLeft: scale(27) }}>{props.text1}</Text>


                <TouchableOpacity
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        width: DeviceWidth / 1.4,
                        padding: 10,
                        borderRadius: 10,
                        backgroundColor: Colors.blueFc,
                    }}
                    onPress={props.Ok}>
                    <Text style={{ color: '#FFFFFF', fontSize: scale(15) }}>{props.label}</Text>
                </TouchableOpacity>


            </View>
        </Modal>
    )
}
export const PopUpBid = (props) => {
    const [text, setText] = React.useState("");
    return (
        <Modal
            isVisible={props.isModalVisible}
            style={{
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 20,
            }}
            animationInTiming={1000}
            animationOutTiming={1000}
            backdropTransitionInTiming={1000}
            backdropTransitionOutTiming={1000}>
            <View style={{ width: DeviceWidth / 1.2, height: scale(200), backgroundColor: "#ffffff", borderRadius: 20, justifyContent: "center", }}>
                <TouchableOpacity
                    style={{
                        justifyContent: 'center',
                        alignItems: 'flex-end',
                        padding: 10,
                        borderRadius: 10,
                        marginTop: -20
                    }}
                    onPress={props.cancel}>
                    <Icon name="close-circle" size={35} color={Colors.red00} />
                </TouchableOpacity>
                <Text style={{ fontSize: scale(15), marginTop: verticalScale(10), marginBottom: verticalScale(10), marginRight: scale(27), marginLeft: scale(27) }}>{'Your Bid'}</Text>
                <View style={{ flexDirection: 'row', borderWidth: 1, borderColor: Colors.whiteF2, borderRadius: 5, marginRight: scale(27), marginLeft: scale(27) }}>
                    <View style={{ flex: 0.3, backgroundColor: Colors.whiteF2, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: scale(15), color: "#707070", textAlign: 'center' }}>{'ETH'}</Text>
                    </View>
                    <View style={{ flex: 0.7 }}>
                        <TextInput
                            style={{
                                height: 20,
                                margin: 12,
                            }}
                            onChangeText={text => props.updateBidText(text)}
                            value={props.bid}
                            placeholder="Minimum eth"
                            keyboardType="numeric"
                        />
                    </View>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            width: DeviceWidth / 1.4,
                            padding: 10,
                            borderRadius: 10,
                            backgroundColor: Colors.blueFc,
                            marginTop: 20
                        }}
                        onPress={props.Ok}>
                        <Text style={{ color: '#FFFFFF', fontSize: scale(15) }}>{props.label}</Text>
                    </TouchableOpacity>

                </View>
            </View>
        </Modal>
    )
}


export const PropertiesPopUP = (props) => {
    const { data = [], setData, firstLine, secondLine, title, decription } = props;
    var Details = [];
    const updateValue = (index, item, res = false) => {
        const name = Object.keys(item);
        // let newList = [
        //     ...data.slice(0, index),
        //     Object.assign({}, data[index], item),
        // ]
        let newList = [
            ...data.slice(0, index),
            Object.assign({}, data[index], item),
        ]
        // if (res) {
        //     if (name.value && name.value > 5) {
        //         res = false
        //     }
        // }
        setData(newList);


    }
    for (let i = 0; i < data.length; i++) {
        Details.push(
            <View style={{ backgroundColor: Colors.whiteFF, shadowColor: Colors.whiteF7, shadowOpacity: 10, elevation: 15, padding: scale(5), borderRadius: scale(5), paddingBottom: scale(10) }}>
                <TouchableOpacity
                    style={{
                        justifyContent: 'center',
                        alignItems: 'flex-end',
                    }}
                    onPress={props.delete}>
                    <Icon name="close-circle" size={24} color={Colors.grayB1} />
                </TouchableOpacity>
                <View style={{
                    margin: scale(10),
                    marginTop: scale(-10)
                }}>
                    <Text style={{
                        fontSize: scale(14),
                    }}>{firstLine}</Text>
                </View>
                <View style={{
                    marginLeft: scale(10),
                    marginRight: scale(10)
                }}>
                    <TextInput
                        style={{
                            borderWidth: 0.5,
                            padding: scale(10),
                            borderColor: Colors.black70,
                            borderRadius: scale(5)
                        }}
                        multiline={true}
                        onChangeText={name => updateValue(i, { name })}
                        value={data[i].name}
                        placeholder={'Enter ' + firstLine}
                    />
                </View>
                <View style={{
                    margin: scale(10),
                }}>
                    <Text style={{
                        fontSize: scale(14),
                    }}>{secondLine}</Text>
                </View>
                {title === 'Add Properties' ? <View style={{
                    marginLeft: scale(10),
                    marginRight: scale(10)
                }}>
                    <TextInput
                        style={{
                            borderWidth: 0.5,
                            padding: scale(10),
                            borderColor: Colors.black70,
                            borderRadius: scale(5)
                        }}
                        multiline={true}
                        onChangeText={value => updateValue(i, { value })}
                        value={data[i].value}
                        placeholder={'Enter ' + secondLine}
                    />
                </View> : <View style={{
                    marginLeft: scale(10),
                    marginRight: scale(10),
                    borderColor: Colors.black70,
                    borderRadius: scale(5),
                    borderWidth: 0.5,
                    flexDirection: 'row'
                }}>
                    <TextInput
                        style={{
                            paddingTop: scale(10),
                            padding: scale(10),

                        }}
                        multiline={true}
                        onChangeText={value => updateValue(i, { value }, true)}
                        value={data[i].value}
                        maxLength={1}
                        placeholder={'Enter ' + secondLine}
                        keyboardType={'number-pad'}
                    />
                    <Text style={{
                        padding: scale(10),
                    }}>out of 5</Text>

                </View>}
            </View>
        );
    }
    return (
        <Modal
            isVisible={props.isModalVisible}
            style={{
                justifyContent: 'center',
                alignItems: 'center',
            }}
        >
            <View style={{ width: DeviceWidth / 1.1, backgroundColor: "#ffffff", borderRadius: 20, padding: 10 }}>
                <ScrollView>
                    <View style={{ flexDirection: 'row', borderBottomColor: Colors.black70, borderBottomWidth: 0.5, marginBottom: verticalScale(10), marginTop: scale(20) }}>
                        <View style={{ flex: 0.9 }}>

                            <Text style={{ fontSize: scale(24), }}>{title}</Text>
                        </View>
                        <View style={{ flex: 0.1 }}>
                            <TouchableOpacity
                                style={{
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}
                                onPress={props.cancel}>
                                <Icon name="close-circle" size={24} color={Colors.red00} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <Text style={{ fontSize: scale(12), marginBottom: verticalScale(10), color: Colors.black70 }}>{decription}</Text>
                    {Details}
                    <View style={{ borderRadius: scale(5), marginLeft: scale(10), marginTop: scale(10) }}>
                        <TouchableOpacity onPress={props.add_another} style={{ width: '40%', borderWidth: 1, borderColor: Colors.blueFc, paddingLeft: scale(10), paddingRight: scale(10), borderRadius: scale(5), padding: scale(5) }}>

                            <Text style={{ textAlign: 'center', fontSize: scale(14), color: Colors.blueFc }}>{'+ Add More'}</Text>
                        </TouchableOpacity>

                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <TouchableOpacity
                            style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                width: DeviceWidth / 1.4,
                                padding: 10,
                                borderRadius: 10,
                                backgroundColor: Colors.blueFc,
                                marginTop: 20
                            }}
                            onPress={props.Ok}>
                            <Text style={{ color: '#FFFFFF', fontSize: scale(15) }}>{props.label}</Text>
                        </TouchableOpacity>

                    </View>
                </ScrollView>
            </View>
        </Modal>
    )
}