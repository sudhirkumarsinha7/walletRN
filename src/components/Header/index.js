/* eslint-disable react-native/no-color-literals */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable sort-keys */
import React, { Component } from 'react';
import {
  BackHandler,
  Dimensions,
  Image,
  Platform,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon1 from 'react-native-vector-icons/MaterialCommunityIcons';

import { Colors, DeviceHeight, DeviceWidth } from '../Common/Style';
import { moderateScale, scale, verticalScale } from '../Scale';


export const Header = (props) => {

  return (
    <View>

      <View style={{}}>
        <View style={{ backgroundColor: Colors.cardBlue, height: Platform.OS === 'ios' ? DeviceWidth / 2.5 : DeviceWidth / 2.5 - 30, padding: 20 }}>
          <View style={{ marginTop: Platform.OS === 'ios' ? 30 : 0, flexDirection: 'row', }}>
            <View style={{ flex: 0.1 }}>

              <TouchableOpacity
                onPress={() => props.navigation.toggleDrawer()}
                style={{
                  right: 10,
                }}>
                <Icon1 name="menu" size={35} color={Colors.whiteFF} />              
              </TouchableOpacity>
            </View>
            <View style={{ flex: 0.8 }}>
              <Text style={{
                color: Colors.whiteFF,
                fontSize: 20,
                fontWeight: 'bold',
                marginTop: -5

              }}>{props.name} </Text>

            </View>

          </View>
        </View>
      </View>

    </View >
  );
}

export const HeaderWithBack = (props) => {
  return (
    <View>

        <View style={{ backgroundColor:'#EE76AD', height: Platform.OS === 'ios' ? DeviceWidth / 5 : DeviceWidth /5 - 30, padding: 5 }}>
          <View style={{ marginTop: Platform.OS === 'ios' ? 30 : 0, flexDirection: 'row', }}>
            <View style={{ flex: 0.1, alignSelf: 'center' }}>

              <TouchableOpacity
                onPress={() => props.navigation.goBack()}
                style={{ width: 100 }}>
                <Icon name="arrow-left" size={25} color={Colors.whiteFF} />
              </TouchableOpacity>
            </View>
            <View style={{ flex: 0.8, justifyContent: 'center' }}>
              <Text style={{
                color: Colors.whiteFF,
                fontSize: 20,
                fontWeight: 'bold',
                marginTop: -5,
              }}>{props.name} </Text>


            </View>
          </View>
      </View>

    </View >
  );
}



