/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  Animated,
  Button,
  Dimensions,
  Image,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import { Colors } from '../Common/Style';
import { moderateScale, scale, verticalScale } from '../Scale';

const SelectionSelect = (props) => {
  return (
    <TouchableOpacity
      onPress={props.onClick}
      activeOpacity={0.8}
      style={{
        flexDirection: 'row',
        marginTop: verticalScale(15),
        marginLeft: scale(5),
      }}>
      {props.button.selected ? (
        <View
          style={{
            borderWidth:1,
            paddingLeft:7,
            paddingRight:7,
            borderColor: props.selectedTextColor || Colors.green,
            backgroundColor:props.selectedTextColor || Colors.green,
            borderRadius:scale(5),
            //marginLeft:scale(10)
            //this.props.button.label === 'Purchase Orders' ? 0 : scale(10),
          }}
        >
          <Text
            style={{
              color: props.unSelectedTextColor || Colors.yellow3B,
              fontSize: props.fontSize || scale(18),
            }}>
            {props.button.label}
          </Text>
        </View>
      ) : (
        <View
          style={{
            borderWidth:1,
            paddingLeft:7,
            paddingRight:7,
            borderColor: props.selectedTextColor || Colors.whiteFF,
            backgroundColor:props.unSelectedTextColor || Colors.red00,
            borderRadius:scale(5),
          }}>
          <Text
            style={{
              color: props.selectedTextColor || Colors.red00,
              fontSize: props.fontSize || scale(18),
            }}>
            {props.button.label}
          </Text>
        </View>
      )}
    </TouchableOpacity>
  );

}
export default SelectionSelect;
export const SelectionSelect1 = (props) => {
  return (
    <TouchableOpacity
      onPress={props.onClick}
      activeOpacity={0.8}
      style={{
        flexDirection: 'row',
        marginTop: verticalScale(15),
        // marginBottom: verticalScale(15),
        // height: scale(35),
        // margin: scale(5),
        marginLeft: scale(10),
        backgroundColor: Colors.whiteFF,
        paddingLeft: 10,
        paddingRight: 10,
        borderRadius: 10,
        padding: 3,
      }}>
      {props.button.selected ? (
        <View
          style={{
            //marginLeft:scale(10)
            //this.props.button.label === 'Purchase Orders' ? 0 : scale(10),
          }}
        >
          <Text
            style={{
              color: props.selectedTextColor || Colors.yellow3B,
              fontSize: props.fontSize || scale(14),
              fontWeight: 'bold'
            }}>
            {props.button.label}
          </Text>

        </View>
      ) : (
        <View
          style={{
            // marginLeft:scale(10)
            //this.props.button.label === 'Purchase Orders' ? 0 : scale(10),
          }}>
          <Text
            style={{
              color: props.unSelectedTextColor || Colors.whiteFF,
              fontSize: props.fontSize || scale(14),
            }}>
            {props.button.label}
          </Text>
        </View>
      )}
    </TouchableOpacity>
  );

}