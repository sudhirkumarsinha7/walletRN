/* eslint-disable no-undef */
/* eslint-disable functional/no-let */
/* eslint-disable react-native/no-color-literals */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import { Slider } from "@rneui/themed";
import React, { useEffect, useState } from 'react';
import { FlatList, Image, ActivityIndicator, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { Colors } from "../Common/Style";
import { PopUpBid, PopUp } from '../PopUp';

import { DeviceHeight, DeviceWidth, imageslist } from '../../config/global'
import { convertETHToUSD, formatDateDDMMYYYYTime } from '../../util/util'
import { CountdownTimer } from '../Common/CountdownTimer';
import { scale } from "../Scale";
const delay = ms => new Promise(res => setTimeout(res, ms));
import { ImageComponent, VideoComponent } from '../Common/heper'

export const CategoryListExpolre = (props) => {
  const { item, SelectedCategory } = props;
  return (<View style={{}}>
    <TouchableOpacity onPress={() => props.onChangeCat(item)} style={{ backgroundColor: SelectedCategory === item.categoryName ? 'blue' : 'white', marginRight: 5, borderRadius: 5, paddingLeft: 10, paddingRight: 10, padding: 5 }} >
      <Text style={{ fontSize: 18, color: SelectedCategory === item.categoryName ? 'white' : 'blue', textAlign: 'center', }}>{item.categoryName.toUpperCase()}</Text>
    </TouchableOpacity>
  </View>
  );
};
export const ExploreList = (props) => {
  const { item = {} } = props;
  const { ownerId = {} } = item;
  // console.log('item '+JSON.stringify(item))
  return (<View style={{ padding: scale(10), backgroundColor: Colors.whiteFF, marginRight: scale(10), marginTop: scale(15), borderRadius: scale(5) }}>

    <TouchableOpacity onPress={() => props.onChange(item)}>

      {item.banner ?
        <Image style={{
          width: DeviceWidth / 1.5, height: 150, borderRadius: 10
        }} source={{ uri: item.banner }} /> : null}
      <View style={{ flexDirection: 'row', borderRadius: 10, }}>
        <View style={{ flex: 0.2 }}>
          {item.thumbnail ? <Image style={{
            width: 40, height: 40, borderRadius: 10
          }} source={{ uri: item.thumbnail }} /> : null}
        </View>
        <View style={{ flex: 0.8, flexDirection: 'row', alignItems: 'center' }}>
          <View style={{ flex: 0.8 }}>
            <Text style={{ fontSize: 18, fontWeight: 'bold', }} numberOfLines={3}>{item.collectionName}</Text>
          </View>
          <View style={{ flex: 0.2, justifyContent: 'center', flexDirection: 'row' }}>
            <AntDesignIcon style={{ alignSelf: 'center' }} name="hearto" size={26} color={Colors.black0} />
            <Text style={{ fontSize: 18, fontWeight: 'bold', marginLeft: 15, alignSelf: 'center', color: Colors.black0 }}>{item.favCount}</Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  </View>
  );
};
export const CategoryExploreList = (props) => {

  const { item = {} } = props;
  const { itemDetails = {} } = item;
  // console.log('item '+JSON.stringify(item))

  return (<View style={{ padding: 10, width: DeviceWidth / 2.3, margin: 10, backgroundColor: '#FFFFFF', borderRadius: 5, borderWidth: 0.5, borderColor: '#EBE7E7' }}>

    <TouchableOpacity onPress={() => props.onChange(item)}>

      <View style={{ alignItems: 'center', justifyContent: 'center' }}>

        {item.fileType ? item.fileType === 'IMAGE' ? (
          <ImageComponent
            url={itemDetails.imageURL}
            imageStyle={{ width: DeviceWidth / 2.5, height: 230, borderRadius: 10 }}
          />
        ) : <VideoComponent
          url={itemDetails.imageURL}
          imageStyle={{ width: DeviceWidth / 2.5, height: 230, borderRadius: 10, backgroundColor: 'black', }}
        /> : null}




      </View>

      <View style={{ flexDirection: 'row' }}>
        <Text style={{ fontSize: 14, fontWeight: 'bold', }}>{itemDetails.name}</Text>

      </View>
      <View style={{ flexDirection: 'row' }}>
        <View style={{ flex: 0.8, flexDirection: 'row' }}>
          <MaterialCommunityIcons style={{ alignSelf: 'center' }} name="ethereum" size={26} color={'#EE76AD'} />
          <Text style={{ fontSize: 12, marginLeft: 5, alignSelf: 'center', color: '#707070' }}>{item.currentPrice + 'ETH'}</Text>

          {/* <Text style={{ fontSize: 12, marginLeft: 5, alignSelf: 'center', color: '#707070' }}>{item.currentPrice + 'ETH'+'('+ convertETHToUSD(item.currentPrice,props.eth_usd) + '$)'}</Text> */}
          {/* <Text style={{ fontSize: 14, marginLeft: 5, alignSelf: 'center', color: '#707070' }}>{'('+ convertETHToUSD(item.currentPrice,props.eth_usd) + '$)'}</Text> */}

        </View>
        <View style={{ flex: 0.3, justifyContent: 'center', flexDirection: 'row' }}>
          <AntDesignIcon style={{ alignSelf: 'center' }} name="hearto" size={18} color={'#DADADA'} />
          <Text style={{ fontSize: 12, fontWeight: 'bold', marginLeft: 15, alignSelf: 'center', color: 'gray' }}>{item.favCount}</Text>
        </View>
      </View>
    </TouchableOpacity>
  </View>
  );
};

export const NftImageView = (props) => {
  const [isModalVisible, setIsModalVisible] = useState(false)
  const [isModalVisible2, setIsModalVisible2] = useState(false)
  const [errorMessage, setErrorMessage] = useState('Issue in API side');
  const [successMessage, setSuccessMessage] = useState('Your Bid placed successfully');
  const [bidText, setBidText] = React.useState("");

  const ok = () => {
    setIsModalVisible(false)
    setIsModalVisible2(false)
  }
  const cancel = () => {
    setIsModalVisible(false)
    setIsModalVisible2(false)
  }
  const placedBid = async () => {
    setIsModalVisible(false)
  }
  const updateBidText = (text) => {
    setBidText(text)
  }
  const { item = {} } = props;
  const { itemDetails = {} } = item;
  console.log('NftImageView item ' + JSON.stringify(item))

  return (<View style={{ padding: 10, width: DeviceWidth / 1.1, margin: 10, backgroundColor: '#FFFFFF', borderRadius: 5, borderWidth: 0.5, borderColor: '#EBE7E7' }}>

    <PopUpBid
      isModalVisible={isModalVisible}
      label="Place Bid"
      Ok={() => placedBid()}
      cancel={() => cancel()}
      updateBidText={(text) => updateBidText(text)}
      bid={bidText}
    />
    <PopUp
      isModalVisible={isModalVisible2}
      image={require('../../assets/success.png')}
      text="Error"
      text1={successMessage}
      label1="Cancel"
      label="Done"
      Ok={() => cancel()}
      cancel={() => cancel()}
    />
    <View style={{ alignItems: 'center', justifyContent: 'center', width: DeviceWidth / 1.2, height: 600, }}>

      {item.fileType ? item.fileType === 'IMAGE' ? (
        <ImageComponent
          url={itemDetails.imageURL}
          imageStyle={{ width: DeviceWidth / 1.2, height: 600, borderRadius: 10 }}
        />
      ) : <VideoComponent
        url={itemDetails.imageURL}
        imageStyle={{ width: DeviceWidth / 1.2, height: 600, borderRadius: 10, backgroundColor: 'black', }}
      /> : null}

      {/* {itemDetails.imageURL ? <Image style={{ width: DeviceWidth / 1.2, height: 600, borderRadius: 10 }} source={{ uri: itemDetails.imageURL }} /> : null} */}
      {item.auctionEndTimestamp ? <View style={{
        position: 'absolute',
        right: 5,
        top: 5,
      }}>
        <CountdownTimer targetDate={item.auctionEndTimestamp} />
      </View> : null}
    </View>

    <View style={{ flexDirection: 'row', marginTop: scale(10), marginBottom: scale(10) }}>
      <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#707070' }}>{itemDetails.name}</Text>

    </View>
    <View style={{ flexDirection: 'row' }}>
      <View style={{ flex: 0.2, flexDirection: 'row' }}>
        <Entypo style={{ alignSelf: 'center' }} name="user" size={30} color={'#EE76AD'} />

      </View>
      <View style={{ flex: 0.4, }}>
        <Text style={{ fontSize: 12, marginLeft: 5, color: '#707070' }}>{'Creater'}</Text>
        <Text style={{ fontSize: 14, marginLeft: 5, color: '#707070' }}>{'XYZ'}</Text>

      </View>
      <View style={{ flex: 0.4, justifyContent: 'center', flexDirection: 'row' }}>
        <FontAwesome5 style={{ alignSelf: 'center' }} name="eye" size={20} color={Colors.blueFF} />
        <Text style={{ fontSize: 12, fontWeight: 'bold', marginLeft: 15, alignSelf: 'center', color: '#B0B0B0' }}>{item.viewCount + ''}</Text>

        <Entypo style={{ alignSelf: 'center', marginLeft: 15, }} name="heart" size={18} color={Colors.red00} />
        <Text style={{ fontSize: 12, fontWeight: 'bold', marginLeft: 15, alignSelf: 'center', color: '#B0B0B0' }}>{item.favCount + ''}</Text>
      </View>
    </View>
    <View>
      <Text style={{ fontSize: 14, marginLeft: 5, color: '#707070', }}>{itemDetails.description}</Text>
    </View>
    <View style={{ marginTop: 5 }}>
      <View style={{ flexDirection: 'row', }}>
        <View style={{ flex: 0.6 }}>
          <Text style={{ fontSize: 20, marginLeft: 5, color: '#707070' }}>{'Current Price'}</Text>
        </View>
        < View style={{ flex: 0.4 }}>
          {item.state === 'ON_AUCTION' ? <TouchableOpacity style={{ alignItems: 'center', backgroundColor: Colors.blueFc, padding: 5, borderRadius: 5 }} onPress={() => setIsModalVisible(true)}>
            <Text style={{ fontSize: 20, marginLeft: 5, color: Colors.whiteFF }}>{'Place Bid'}</Text>

          </TouchableOpacity> : item.state === 'ON_SALE' ? <TouchableOpacity style={{ alignItems: 'center', backgroundColor: Colors.blueFc, padding: 5, borderRadius: 5 }} onPress={() => setIsModalVisible2(true)}>
            <Text style={{ fontSize: 20, marginLeft: 5, color: Colors.whiteFF }}>{'Buy now'}</Text>

          </TouchableOpacity> : null}
        </View>
      </View>
      <View style={{ flexDirection: 'row', }}>
        <View style={{ flex: 0.7, flexDirection: 'row', }}>

          <MaterialCommunityIcons style={{ alignSelf: 'center' }} name="ethereum" size={26} color={'#EE76AD'} />
          <Text style={{ fontSize: 12, marginLeft: 5, alignSelf: 'center', color: '#707070' }}>{item.currentPrice + 'ETH'}</Text>

          <Text style={{ fontSize: 14, marginLeft: 5, alignSelf: 'center', color: '#707070' }}>{'(' + convertETHToUSD(item.currentPrice, props.eth_usd) + '$)'}</Text>
        </View>

        <View style={{ flex: 0.3 }}>

        </View>

      </View>
    </View>
  </View>
  );
};
export const NftInfo = (props) => {
  const { item = {} } = props;
  const { itemDetails = {} } = item;
  return (<View style={{ padding: 10, width: DeviceWidth / 1.1, margin: 10, borderRadius: 5, borderWidth: 0.5, borderColor: '#EBE7E7' }}>


    <View style={{ flexDirection: 'row' }}>
      <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#707070' }}>{itemDetails.name}</Text>

    </View>
    <PropertisInfo
      properties={itemDetails.properties || []}
    />
    <SliderScreen
      levels={itemDetails.levels || []}
    />
    <StatusScreen
      stats={itemDetails.stats || []}
    />
  </View>
  );
};
export const PropertisInfo = (props) => {
  const [isShow, setShow] = useState(false);
  const eachProperties = (item) => {
    return (<View style={{ backgroundColor: '#0364ff', padding: 10, margin: 5, borderRadius: 10 }}>
      <Text>{item.name}</Text>
      <Text>{item.value + ''}</Text>
    </View>)
  }
  let newPropeties = props.properties.filter((each) => {
    if (each.value) {
      return each
    }
  })
  return (
    <TouchableOpacity onPress={() => { setShow(!isShow) }}>
      <View style={{ backgroundColor: '#FFFFFF', padding: 10, marginTop: 5, borderRadius: 5 }}>

        <Text>Propertis</Text>
        {isShow ? (<FlatList
          onEndReachedThreshold={0.7}
          keyExtractor={(item, index) => index.toString()}
          data={newPropeties || []}
          numColumns={2}
          renderItem={({ item }) => eachProperties(item)}
        />) : null}

      </View>
    </TouchableOpacity>

  )
}
export const SliderScreen = (props) => {
  const [isShow, setShow] = useState(false);
  const eachSlider = (item) => {
    return (<View style={{ backgroundColor: Colors.whiteFF, borderRadius: 10, marginTop: 5, borderRadius: 5 }}>
      <Text>{item.name}</Text>
      {item.value ? <Slider
        value={item.value}
        onValueChange={console.log('item.value')}
        maximumValue={5}
        minimumValue={0}
        step={item.value}
        disabled={true}
        trackStyle={{ height: 10, borderRadius: 5 }}
        thumbStyle={{ height: 10, width: 10, backgroundColor: Colors.blueFc }}
        maximumTrackTintColor={Colors.grayB1}
        minimumTrackTintColor={Colors.blueFc}
      /> : null}
    </View>)
  }
  let newLevel = props.levels.filter((each) => {
    if (each.value) {
      return each
    }
  })
  return (
    <TouchableOpacity onPress={() => { setShow(!isShow) }}
      style={{ backgroundColor: '#FFFFFF', padding: 10, marginTop: 5, borderRadius: 5 }}>
      <Text>Levels</Text>
      {isShow ? <FlatList
        onEndReachedThreshold={0.7}
        keyExtractor={(item, index) => index.toString()}
        data={newLevel || []}
        renderItem={({ item }) => eachSlider(item)}
      /> : null}
    </TouchableOpacity>

  )
}
export const StatusScreen = (props) => {
  const [isShow, setShow] = useState(false);
  const eachSlider = (item) => {
    return (<View style={{ flexDirection: 'row', padding: scale(7) }}>
      <Text style={{ flex: 0.8 }}>{item.name}</Text>
      <Text style={{ flex: 0.2 }}>{item.value + ' Of 5'}</Text>
    </View>)
  }
  let newStatus = props.stats.filter((each) => {
    if (each.value) {
      return each
    }
  })
  return (
    <TouchableOpacity onPress={() => { setShow(!isShow) }}
      style={{ backgroundColor: '#FFFFFF', padding: 10, marginTop: 5 }}>
      <Text>Stats</Text>
      {isShow ? <FlatList
        onEndReachedThreshold={0.7}
        keyExtractor={(item, index) => index.toString()}
        data={newStatus || []}
        renderItem={({ item }) => eachSlider(item)}
      /> : null}
    </TouchableOpacity>

  )
}
export const NftBidHistory = (props) => {
  const { item = {} } = props;
  const { history = [] } = item;

  const eachHistory = (item1) => {
    const { sourceParty = {} } = item1
    console.log('sourceParty item ' + JSON.stringify(item1))
    return (<View>
      <View style={{ flexDirection: 'row', marginTop: 5, backgroundColor: Colors.whiteFF, padding: 5 }}>
        <View style={{ flex: 0.2, flexDirection: 'row' }}>
          <Entypo style={{ alignSelf: 'center' }} name="user" size={30} color={'#EE76AD'} />
        </View>
        <View style={{ flex: 0.8, flexDirection: 'row', }}>
          <View style={{ flex: 0.7 }}>

            <Text style={{ fontSize: 12, marginLeft: 5, color: '#707070' }}>{sourceParty.walletAddress}</Text>
            <Text style={{ fontSize: 12, marginLeft: 5, color: '#707070' }}>{formatDateDDMMYYYYTime(item1.transactionTime)}</Text>
          </View>
          <View style={{ flexDirection: 'row', flex: 0.3 }}>

            {/* <Text style={{ fontSize: 12, marginLeft: 5, color: '#707070' }}>{item1.transactionTime}</Text> */}
            <MaterialCommunityIcons style={{ alignSelf: 'center' }} name="ethereum" size={26} color={'#EE76AD'} />

            <Text style={{ fontSize: 12, marginLeft: 5, alignSelf: 'center', color: '#707070' }}>{'1 ETH'}</Text>

          </View>
        </View>

      </View>
    </View>)
  }
  return (<View style={{ padding: 10, width: DeviceWidth / 1.1, margin: 10, borderRadius: 5, borderWidth: 0.5, borderColor: '#EBE7E7' }}>
    <FlatList
      onEndReachedThreshold={0.7}
      keyExtractor={(item, index) => index.toString()}
      data={history || []}
      renderItem={({ item }) => eachHistory(item)}
    />
  </View>
  );
};




export const UserCategoryExploreList = (props) => {

  const { Item = {} } = props;
  const { item = {} } = Item;

  const { itemDetails = {} } = item;
  console.log('item '+JSON.stringify(item))

  return (<View style={{ padding: 10, width: DeviceWidth / 2.3, margin: 10, backgroundColor: '#FFFFFF', borderRadius: 5, borderWidth: 0.5, borderColor: '#EBE7E7' }}>

    <TouchableOpacity onPress={() => props.onChange(item)}>

      <View style={{ alignItems: 'center', justifyContent: 'center' }}>

        {item.fileType ? item.fileType === 'IMAGE' ? (
          <ImageComponent
            url={itemDetails.imageURL}
            imageStyle={{ width: DeviceWidth / 2.5, height: 230, borderRadius: 10 }}
          />
        ) : <VideoComponent
          url={itemDetails.imageURL}
          imageStyle={{ width: DeviceWidth / 2.5, height: 230, borderRadius: 10, backgroundColor: 'black', }}
        /> : null}




      </View>

      <View style={{ flexDirection: 'row' }}>
        <Text style={{ fontSize: 14, fontWeight: 'bold', }}>{itemDetails.name}</Text>

      </View>
      <View style={{ flexDirection: 'row' }}>
        <View style={{ flex: 0.8, flexDirection: 'row' }}>
          <MaterialCommunityIcons style={{ alignSelf: 'center' }} name="ethereum" size={26} color={'#EE76AD'} />
          <Text style={{ fontSize: 12, marginLeft: 5, alignSelf: 'center', color: '#707070' }}>{item.currentPrice + 'ETH'}</Text>

          {/* <Text style={{ fontSize: 12, marginLeft: 5, alignSelf: 'center', color: '#707070' }}>{item.currentPrice + 'ETH'+'('+ convertETHToUSD(item.currentPrice,props.eth_usd) + '$)'}</Text> */}
          {/* <Text style={{ fontSize: 14, marginLeft: 5, alignSelf: 'center', color: '#707070' }}>{'('+ convertETHToUSD(item.currentPrice,props.eth_usd) + '$)'}</Text> */}

        </View>
        <View style={{ flex: 0.3, justifyContent: 'center', flexDirection: 'row' }}>
          <AntDesignIcon style={{ alignSelf: 'center' }} name="hearto" size={18} color={'#DADADA'} />
          <Text style={{ fontSize: 12, fontWeight: 'bold', marginLeft: 15, alignSelf: 'center', color: 'gray' }}>{item.favCount}</Text>
        </View>
      </View>
    </TouchableOpacity>
  </View>
  );
};