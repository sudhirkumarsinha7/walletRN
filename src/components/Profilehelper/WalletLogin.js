/* eslint-disable no-unused-vars */
/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Button,
  ScrollView,
} from 'react-native';
// import Web3authReactNativeSdk, {
//   LOGIN_PROVIDER,
//   MfaLevel,
//   Web3authNetwork,
// } from '@web3auth/react-native-sdk';

import Web3Auth, { LOGIN_PROVIDER, OPENLOGIN_NETWORK, State } from "@web3auth/react-native-sdk";

import * as WebBrowser from "expo-web-browser";


import {connect} from 'react-redux';
import {getUserInfo} from '../../redux/action/auth';
const Web3authReactNativeSdk = new Web3Auth(WebBrowser, {
  clientId:'BADRfS8jlrBLxJ5uGOXRrK25rC59pM4RpM50zGXRBVpTaVUgyP0zzn4ZnDD2ozqwU-zCDl4LJf28Jq2wBuQtuFA',
network: 'testnet',
redirectUrl: 'com.statwig.tollytokens://auth',
});
var ethers = require('ethers');
const WalletLogin = props => {
  const [loginResult, setLoginResult] = useState('');
  useEffect(() => {


    
    // Web3authReactNativeSdk.init({
    //   clientId:
    //     'BADRfS8jlrBLxJ5uGOXRrK25rC59pM4RpM50zGXRBVpTaVUgyP0zzn4ZnDD2ozqwU-zCDl4LJf28Jq2wBuQtuFA',
    //   // network: Web3authNetwork.DEVELOPMENT,
    //   network: 'testnet',

    //   redirectUrl: 'com.statwig.tollytokens://auth',
    //   // browserRedirectUri: 'https://scripts.toruswallet.io/redirect.html',

    //     // redirectUrl: 'com.statwig.nft://auth/ios/com.statwig.nft/callback',
    // })
    //   .then(result => console.log(`success: ${result}`))
    //   .catch(err => console.log(`error: ${err}`));
  }, []);
  const getUserInfoDetails = async result => {
    console.log('getUserInfoDetails result: ' + JSON.stringify(result));

    setLoginResult(result);
    let privateKey = result.privKey;
    let provider = ethers.getDefaultProvider();
    let walletWithProvider = new ethers.Wallet(privateKey, provider);
    // let wallet = new ethers.Wallet(privateKey);
    console.log('walletWithProvider: ' + JSON.stringify(walletWithProvider));
    // result.wallet = wallet;
    result.provider = walletWithProvider;
    await props.getUserInfo(result);
  };
  console.log('props.userInfo ' + JSON.stringify(props.userInfo));
  return (
    <ScrollView>
      <View style={styles.container}>
        <Text
          style={{
            fontSize: 20,
            fontWeight: 'bold',
            color: 'red',
            alignSelf: 'center',
          }}>
          Login With Wallet
        </Text>
        <View style={styles.box}>
          <TouchableOpacity
            onPress={() =>
              Web3authReactNativeSdk.login({
                provider: LOGIN_PROVIDER.GOOGLE,
                redirectUrl: 'com.statwig.tollytokens://auth',

              })
                .then(result => {
                  getUserInfoDetails(result);
                })
                .catch(err => console.log(`error: ${err}`))
            }>
            <Text style={{fontSize: 20, fontWeight: 'bold', color: 'blue'}}>
              Google
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.box}>
          <TouchableOpacity
            onPress={() =>
              Web3authReactNativeSdk.login({
                provider: LOGIN_PROVIDER.FACEBOOK,
              })
                .then(result => {
                  getUserInfoDetails(result);
                })
                .catch(err => console.log(`error: ${err}`))
            }>
            <Text style={{fontSize: 20, fontWeight: 'bold', color: 'blue'}}>
              FACEBOOK
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.box}>
          <TouchableOpacity
            onPress={() =>
              Web3authReactNativeSdk.login({
                provider: LOGIN_PROVIDER.REDDIT,
              })
                .then(result => {
                  getUserInfoDetails(result);
                })
                .catch(err => console.log(`error: ${err}`))
            }>
            <Text style={{fontSize: 20, fontWeight: 'bold', color: 'blue'}}>
              REDDIT
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.box}>
          <TouchableOpacity
            onPress={() =>
              Web3authReactNativeSdk.login({
                provider: LOGIN_PROVIDER.DISCORD,
              })
                .then(result => {
                  getUserInfoDetails(result);
                })
                .catch(err => console.log(`error: ${err}`))
            }>
            <Text style={{fontSize: 20, fontWeight: 'bold', color: 'blue'}}>
              DISCORD
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.box}>
          <TouchableOpacity
            onPress={() =>
              Web3authReactNativeSdk.login({
                provider: LOGIN_PROVIDER.TWITCH,
              })
                .then(result => {
                  getUserInfoDetails(result);
                })
                .catch(err => console.log(`error: ${err}`))
            }>
            <Text style={{fontSize: 20, fontWeight: 'bold', color: 'blue'}}>
              TWITCH
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.box}>
          <TouchableOpacity
            onPress={() =>
              Web3authReactNativeSdk.login({
                provider: LOGIN_PROVIDER.APPLE,
              })
                .then(result => {
                  getUserInfoDetails(result);
                })
                .catch(err => console.log(`error: ${err}`))
            }>
            <Text style={{fontSize: 20, fontWeight: 'bold', color: 'blue'}}>
              Apple
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.box}>
          <TouchableOpacity
            onPress={() =>
              Web3authReactNativeSdk.login({
                provider: LOGIN_PROVIDER.EMAIL_PASSWORDLESS,
                relogin: true,
                extraLoginOptions: {
                  login_hint: 'michael@tor.us',
                },
              })
                .then(result => {
                  getUserInfoDetails(result);
                })
                .catch(err => console.log(`error: ${err}`))
            }>
            <Text style={{fontSize: 20, fontWeight: 'bold', color: 'blue'}}>
              Email
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.box}>
          <TouchableOpacity
            onPress={() =>
              Web3authReactNativeSdk.login({extraLoginOptions: {}})
                .then(result => {
                  getUserInfoDetails(result);
                })
                .catch(err => console.log(`error: ${err}`))
            }>
            <Text style={{fontSize: 20, fontWeight: 'bold', color: 'blue'}}>
              Web3Auth
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.box}>
          <TouchableOpacity
            onPress={() =>
              Web3authReactNativeSdk.login({
                provider: LOGIN_PROVIDER.LINE,
              })
                .then(result => {
                  getUserInfoDetails(result);
                })
                .catch(err => console.log(`error: ${err}`))
            }>
            <Text style={{fontSize: 20, fontWeight: 'bold', color: 'blue'}}>
              LINE
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.box}>
          <TouchableOpacity
            onPress={() =>
              Web3authReactNativeSdk.login({
                provider: LOGIN_PROVIDER.GITHUB,
              })
                .then(result => {
                  getUserInfoDetails(result);
                })
                .catch(err => console.log(`error: ${err}`))
            }>
            <Text style={{fontSize: 20, fontWeight: 'bold', color: 'blue'}}>
              GITHUB
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.box}>
          <TouchableOpacity
            onPress={() =>
              Web3authReactNativeSdk.login({
                provider: LOGIN_PROVIDER.LINE,
              })
                .then(result => {
                  getUserInfoDetails(result);
                })
                .catch(err => console.log(`error: ${err}`))
            }>
            <Text style={{fontSize: 20, fontWeight: 'bold', color: 'blue'}}>
              LINE
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.box}>
          <TouchableOpacity
            onPress={() =>
              Web3authReactNativeSdk.login({
                provider: LOGIN_PROVIDER.GITHUB,
              })
                .then(result => {
                  getUserInfoDetails(result);
                })
                .catch(err => console.log(`error: ${err}`))
            }>
            <Text style={{fontSize: 20, fontWeight: 'bold', color: 'blue'}}>
              GITHUB
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.box}>
          <TouchableOpacity
            onPress={() =>
              Web3authReactNativeSdk.login({
                provider: LOGIN_PROVIDER.LINKEDIN,
              })
                .then(result => {
                  getUserInfoDetails(result);
                })
                .catch(err => console.log(`error: ${err}`))
            }>
            <Text style={{fontSize: 20, fontWeight: 'bold', color: 'blue'}}>
              LINKEDIN
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.box}>
          <TouchableOpacity
            onPress={() =>
              Web3authReactNativeSdk.login({
                provider: LOGIN_PROVIDER.KAKAO,
              })
                .then(result => {
                  getUserInfoDetails(result);
                })
                .catch(err => console.log(`error: ${err}`))
            }>
            <Text style={{fontSize: 20, fontWeight: 'bold', color: 'blue'}}>
              KAKAO
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.box}>
          <TouchableOpacity
            onPress={() =>
              Web3authReactNativeSdk.login({
                provider: LOGIN_PROVIDER.TWITTER,
              })
                .then(result => {
                  getUserInfoDetails(result);
                })
                .catch(err => console.log(`error: ${err}`))
            }>
            <Text style={{fontSize: 20, fontWeight: 'bold', color: 'blue'}}>
              TWITTER
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.box}>
          <TouchableOpacity
            onPress={() =>
              Web3authReactNativeSdk.login({
                provider: LOGIN_PROVIDER.WEIBO,
              })
                .then(result => {
                  getUserInfoDetails(result);
                })
                .catch(err => console.log(`error: ${err}`))
            }>
            <Text style={{fontSize: 20, fontWeight: 'bold', color: 'blue'}}>
              WEIBO
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.box}>
          <TouchableOpacity
            onPress={() =>
              Web3authReactNativeSdk.login({
                provider: LOGIN_PROVIDER.EMAIL_PASSWORDLESS,
              })
                .then(result => {
                  getUserInfoDetails(result);
                })
                .catch(err => console.log(`error: ${err}`))
            }>
            <Text style={{fontSize: 20, fontWeight: 'bold', color: 'blue'}}>
              EMAIL_PASSWORDLESS
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.box}>
          <TouchableOpacity
            onPress={() =>
              Web3authReactNativeSdk.login({
                provider: LOGIN_PROVIDER.WECHAT,
              })
                .then(result => {
                  getUserInfoDetails(result);
                })
                .catch(err => console.log(`error: ${err}`))
            }>
            <Text style={{fontSize: 20, fontWeight: 'bold', color: 'blue'}}>
              WECHAT
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.box}>
          <TouchableOpacity
            onPress={() =>
              Web3authReactNativeSdk.login({
                provider: LOGIN_PROVIDER.JWT,
              })
                .then(result => {
                  getUserInfoDetails(result);
                })
                .catch(err => console.log(`error: ${err}`))
            }>
            <Text style={{fontSize: 20, fontWeight: 'bold', color: 'blue'}}>
              JWT
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.box}>
          <TouchableOpacity
            onPress={() =>
              Web3authReactNativeSdk.login({
                provider: LOGIN_PROVIDER.WEBAUTHN,
              })
                .then(result => {
                  getUserInfoDetails(result);
                })
                .catch(err => console.log(`error: ${err}`))
            }>
            <Text style={{fontSize: 20, fontWeight: 'bold', color: 'blue'}}>
              WEBAUTHN
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.box}>
          <TouchableOpacity
            onPress={() =>
              Web3authReactNativeSdk.login({
                provider: LOGIN_PROVIDER.GOOGLE,
                mfaLevel: MfaLevel.MANDATORY,
              })
                .then(result => {
                  getUserInfoDetails(result);
                })
                .catch(err => console.log(`error: ${err}`))
            }>
            <Text style={{fontSize: 20, fontWeight: 'bold', color: 'blue'}}>
              Login with Google with mandatory MFA
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.box}>
          <TouchableOpacity
            onPress={() =>
              Web3authReactNativeSdk.login({
                provider: LOGIN_PROVIDER.GOOGLE,
                mfaLevel: MfaLevel.NONE,
              })
                .then(result => {
                  getUserInfoDetails(result);
                })
                .catch(err => console.log(`error: ${err}`))
            }>
            <Text style={{fontSize: 20, fontWeight: 'bold', color: 'blue'}}>
              Login with Google with none MFA
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.box}>
          <TouchableOpacity
            onPress={() =>
              Web3authReactNativeSdk.logout({})
                .then(result => setLoginResult(''))
                .catch(err => console.log(`error: ${err}`))
            }>
            <Text style={{fontSize: 20, fontWeight: 'bold', color: 'red'}}>
              Logout
            </Text>
          </TouchableOpacity>
        </View>
        <Text>{JSON.stringify(props.userInfo)}</Text>
      </View>
    </ScrollView>
  );
};
function mapStateToProps(state) {
  return {
    top_seller_list: state.home.top_seller_list,
    popular_collection_list: state.home.popular_collection_list,
    category_list: state.home.category_list,
    flter_collection: state.home.flter_collection,
    item_list: state.home.item_list,
    userInfo: state.auth.userInfo,
  };
}
export default connect(mapStateToProps, {getUserInfo})(WalletLogin);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // alignItems: 'center',
    margin: 10,
    justifyContent: 'center',
  },
  box: {
    height: 40,
    marginTop: 15,
  },
  text: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 16,
    marginTop: 20,
    width: '100%',
    backgroundColor: 'white',
  },
});
