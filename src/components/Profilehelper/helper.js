import React, { useEffect } from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Image, Text, ImageBackground } from 'react-native';
import { imageslist, DeviceWidth, DeviceHeight, userNftList } from '../../config/global'
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import { Colors } from "../Common/Style";

export const UserCollectionList = (props) => {
    const { item, selectedUserList, SetUserList } = props;
    return (<View style={{ marginRight: 15 }} >
        <TouchableOpacity
            onPress={() => SetUserList(item.name)}
            style={{ borderWidth: 1, borderColor: Colors.blueFc, padding: 5, borderRadius: 5, backgroundColor: selectedUserList === item.name ? Colors.blueFc : Colors.whiteFF }} >
            <Text style={{ fontSize: 14, color: selectedUserList === item.name ? Colors.whiteFF : Colors.blueFc, textAlign: 'center', }}>{item.name}</Text>
        </TouchableOpacity>
    </View>
    );
};
export const ExploreList = (props) => {
    const { item = {}, } = props;
    const { ownerId = {} } = item;
    return (<View style={{ padding: 5, }} >
        <TouchableOpacity >
            <Image
                style={{
                    width: 180, height: 100, borderRadius: 10
                }}
                source={item.categoryName === 'art' ? imageslist[0].image : item.categoryName === 'Digital' ? imageslist[1].image : imageslist[0].image}
            />
            <View style={{ flexDirection: 'row', backgroundColor: 'gray', borderRadius: 10 }}>
                <View style={{ flex: 0.7 }}>
                    <Text style={{ fontSize: 16, color: Colors.whiteFF, fontWeight: 'bold', marginLeft: 15 }}>{item.collectionName}</Text>
                    <Text style={{ fontSize: 16, color: Colors.whiteFF, fontWeight: 'bold', marginLeft: 15 }}>{ownerId.firstName}</Text>
                </View>
                <View style={{ flex: 0.2, justifyContent: 'center', flexDirection: 'row' }}>
                    <AntDesignIcon
                        style={{ alignSelf: 'center' }}
                        name="hearto"
                        size={26}
                        color={Colors.whiteFF}
                    />

                    <Text style={{ fontSize: 16, color: Colors.whiteFF, fontWeight: 'bold', marginLeft: 15, alignSelf: 'center' }}>{item.favCount}</Text>

                </View>
            </View>
        </TouchableOpacity>
    </View>
    );
};