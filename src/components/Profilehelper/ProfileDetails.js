/* eslint-disable no-unused-vars */
/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect } from 'react';
import setAuthToken from '../../config/setAuthToken';

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Button,
  ScrollView,
  Image,
  FlatList,
} from 'react-native';
import { connect } from 'react-redux';
import {
  imageslist,
  DeviceWidth,
  DeviceHeight,
  userInfo,
  userNftList,
} from '../../config/global';
import { UserCollectionList, ExploreList } from './helper';
import {
  UserCategoryExploreList,
  CategoryListExpolre,
} from '../../components/Explore/helper';
import { scale } from '../Scale';
import { Colors } from '../Common/Style';
import Logout_PopUp from '../../components/Logout_PopUp';
import Verify_PopUp from '../../components/Verify_PopUp';
import {
  LogIn,
  Register,
  ResetUserInfo,
  DeleteAccount,
} from '../../redux/action/auth';
import {
  getNftDetails,
} from '../../redux/action/home';
import {
 getUserCreatedList,
 getUserUnlisted,
 getUserPurchaseList,
 getUserFavorites,
 getUserCollection

} from '../../redux/action/user';
import WalletConnectProvider from '@walletconnect/web3-provider';
import { useWalletConnect, withWalletConnect } from '@walletconnect/react-native-dapp';
import { NavigationContainer, useIsFocused } from '@react-navigation/native';
import { ethers } from 'ethers';
const testData =[{"_id":"6315bac823ff915c692693a3","item":{"_id":"63073dbdad514188c81164c8","itemDetails":{"name":"Mahesh babu ","imageURL":"https://ipfs.io/ipfs/QmdJHZE4SYfYxDHaSCdmMu3qEA4rDhyxRtHzqhUUYsMhSk","externalLink":null,"description":"Tollywood superstar, producer director and businessman","properties":[{"name":"BACKGROUND","value":"WHITE"},{"name":"NFT TYPE","value":"AVATAR"}],"levels":[{"name":"","value":""}],"stats":[{"name":"","value":""}],"unlockableContent":false,"explicitContent":false},"tokenId":"122","currentOwner":"63073d0a73e2fa95efd86e86","currentPrice":230,"INRPrice":2300,"creator":"63073d0a73e2fa95efd86e86","resale":false,"state":"ON_SALE","viewCount":4,"favCount":1,"fileType":"IMAGE","history":["63073dbdad514188c81164cc","63073e25ad514188c81164da"],"redeemable":false,"createdAt":"2022-08-25T09:15:41.431Z","updatedAt":"2022-09-05T09:00:56.069Z","id":"ITM-10068","__v":0,"category":"62a30d8f74d88f3f97104a70","collectionRef":"62baf9e3255475434773cb51","currency":"STWG","nftId":95,"royaltyInPercent":5,"currentOwnerWalletID":"0xF71021E852BF8626b8A418C0A05E2A33B9385f10","creatorWalletID":"0xF71021E852BF8626b8A418C0A05E2A33B9385f10"},"collectionRef":null,"eventType":"FAVORITE","user":"62bee1c13837e30d62111465","createdAt":"2022-09-05T09:00:56.067Z","updatedAt":"2022-09-05T09:00:56.067Z","__v":0}]
const ProfileDetials = props => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isVerifyModel, setIsVerifyModel] = useState(false);

  const isFocused = useIsFocused();
  const connector = useWalletConnect(); // valid
  const [selectedUserList, SetUserList] = useState('User Created');
  const [connected, setconnected] = useState(false)
  useEffect(() => {
  //  if(props.userInfo && props.userInfo.token){
  //   setAuthToken(props.userInfo.token)
  //   setIsVerifyModel(false)
  //  }else{
  //   setIsVerifyModel(true)
  //  }
  const Id= '1'
  props.getUserCreatedList(Id),
  props.getUserUnlisted(Id),
  props.getUserPurchaseList(Id),
  props.getUserFavorites(Id),
  props.getUserCollection(Id)
 }, [isFocused]);
  const _signout = async () => {
    setIsModalVisible(true)
  };
  const _DeleteProf = async()=>{
    setIsModalVisible(false)
    connector.killSession()
    props.DeleteAccount()
    props.ResetUserInfo()
    // alert('Not Implemented yet')

  }
  const verifyAuth = async () => {
    // const message = "\x19Ethereum Signed Message:\nAn amazing message, for use with MetaMask!";
    const message = "An amazing message, for use with MetaMask!";
    let signatures;
    const provider = new WalletConnectProvider({
      bridge: "https://bridge.walletconnect.org",
      rpc: { 1: "https://rpc.ankr.com/eth", 3: "https://ropsten.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161" },
      infuraId: "8043bb2cf99347b1bfadfb233c5325c0",
      connector: connector,
      connected: true,
      qrcode: false,
    });
    await provider.enable();
    const ethers_provider = new ethers.providers.Web3Provider(provider);
    const signer = await ethers_provider.getSigner();

    signatures = await signer.signMessage(message);
    console.log('signatures ', signatures);
    let RegisterData = {
      walletAddress: connector.accounts[0],
    }
    let LoginData = {
      walletId: connector.accounts[0],
      signature: signatures,
      message: message,
    }
    await props.Register(RegisterData);
    await props.LogIn(LoginData)
    setIsVerifyModel(false)

  };
  const Cancel = () => {
    setIsModalVisible(false)
  };
  const eachCat = each => {
    // console.log('eachCat ' + JSON.stringify(each));
    return (

      <UserCollectionList
        item={each}
        selectedUserList={selectedUserList}
        SetUserList={SetUserList}
      />
    );
  };
  
  const eachExplore = each => {
    console.log('eachExplore ' + JSON.stringify(each));
    return (
      <View>
       <UserCategoryExploreList Item={each}
          onChange={onChangeItemCategory}
          eth_usd={props.eth_usd} />
      </View>
    );
  };
  const onChangeItemCategory = async (item) => {
    console.log('onChangeItemCategory item'+JSON.stringify(item))
    await props.getNftDetails(item._id)
    // props.navigation.navigate('ViewNft',{nftDetails:item})
  }
  // console.log('isVerifyModel ' + isVerifyModel);
  console.log('(props.userInfo ' + JSON.stringify(props.user_unlisted));

  return (
    <View style={styles.container}>
      <Logout_PopUp
          Cancel={Cancel}
          Logout={_DeleteProf}
          isModalVisible={isModalVisible}
        />
        <Verify_PopUp
          Verify={()=>verifyAuth()}
          isModalVisible={isVerifyModel}
        />
      <View
        style={{
          backgroundColor: Colors.whiteFF,
          borderRadius: scale(10),
        }}>
        <TouchableOpacity onPress={()=>_signout()} style={{ width: scale(110), height: scale(25), backgroundColor: 'red', padding: 2, borderRadius: scale(10), marginTop: scale(10), marginLeft: scale(10),justifyContent:'center' }}>
          <Text style={{ color: Colors.whiteFF, fontWeight: 'bold',marginLeft:scale(5),textAlign:'center' }}>Delete Account</Text>

        </TouchableOpacity>
        <View style={{
          alignItems: 'center',
          justifyContent: 'center',
        }}>
          <Image
            style={{
              width: 150,
              height: 150,
              borderRadius: 75,
              marginTop: -30,
              backgroundColor: 'green',
            }}
            source={imageslist[6].image}
          />
            <Text style={{ fontSize: 14,margin: 10 }}  numberOfLines={1}
            ellipsizeMode="tail">{'Wallet Address: '+connector.accounts[0]}</Text>
        </View>
        <Text style={{ fontSize: 18, margin: 10 }}>{userInfo.comment}</Text>

      </View>

      <View style={{ marginTop: scale(20),marginBottom:scale(20) }}>
        <FlatList
          onEndReachedThreshold={0.7}
          horizontal={true}
          keyExtractor={(item, index) => index.toString()}
          data={userNftList}
          renderItem={({ item }) => eachCat(item)}
        />
      </View>
      <View>
          <FlatList
            onEndReachedThreshold={0.7}
            horizontal={false}
            numColumns={2}
            keyExtractor={(item, index) => index.toString()}
            data={testData}
            // data={ selectedUserList==='User Created'? props.user_created:selectedUserList==='User Purchase'?props.user_purchase:selectedUserList==='User Unlisted'?props.user_unlisted : selectedUserList==='My Collection'?props.user_collection :selectedUserList==='Favarotes'?props.user_fav :[]}
            renderItem={({ item }) => eachExplore(item)}
          />
        </View>
    </View>
  );
};
function mapStateToProps(state) {
  return {
    top_seller_list: state.home.top_seller_list,
    popular_collection_list: state.home.popular_collection_list,
    category_list: state.home.category_list,
    flter_collection: state.home.flter_collection,
    item_list: state.home.item_list,
    userInfo: state.auth.userInfo,
    user_fav:state.user.user_fav,
    user_collection:state.user.user_collection,
    user_purchase:state.user.user_purchase,
    user_unlisted:state.user.user_unlisted,
    user_created:state.user.user_created,
    eth_usd: state.home.eth_usd,
    walletAddress:state.user.walletAddress

  };
}
export default connect(mapStateToProps, {  LogIn,
  Register,ResetUserInfo,DeleteAccount,getUserCreatedList,
  getUserUnlisted,
  getNftDetails,
  getUserPurchaseList,
  getUserFavorites,
  getUserCollection})(ProfileDetials);
const styles = StyleSheet.create({
  container: {
    margin: 10, marginTop: Platform.OS === 'ios' ? scale(40) : 0,
  },
});
