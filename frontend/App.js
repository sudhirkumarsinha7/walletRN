/* eslint-disable react-native/no-inline-styles */
import 'react-native-gesture-handler';
import { createTheme, ThemeProvider } from '@rneui/themed';
import * as React from 'react';
import { StatusBar, Text, View, Platform } from 'react-native';
import { Provider } from 'react-redux';

import Store from '../src/redux/Store';
import Root from '../src/route/Navigation';

import { WebView } from 'react-native-webview';

const theme = createTheme({
  lightColors: {
    primary: 'black',
  },
  // eslint-disable-next-line sort-keys
  darkColors: {
    primary: '#000',
  },
});


function App() {
  return (
    //  <Root/>
    <Provider store={Store}>
      <ThemeProvider theme={theme}>
        <StatusBar backgroundColor="#fff" barStyle="dark-content" />
        <View style={{flex: 1}}>
          <Root />
        </View>
      </ThemeProvider>
    </Provider>
  );
}
// function App() {

//   return <WebView source={{ uri: 'https://test.tollytokens.com/' }}
//     style={{ marginTop: Platform.OS === 'ios' ? 40 : 0 }} />

// }

export default App;
